package com.agileai.hotweb.renders;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AjaxRenderer extends ViewRenderer {
	private String codeType = "text/xml";
	private String responseText = null;
	public AjaxRenderer(String responseText){
		this.responseText = responseText;
	}
	public AjaxRenderer(String responseText,String codeType){
		this.responseText = responseText;
		this.codeType = codeType;
	}
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws IOException{
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Pragma","no-cache");
		response.setDateHeader("Expires",0); 
		response.setContentType(this.codeType+";charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.write(responseText);
		out.close();
	}
}

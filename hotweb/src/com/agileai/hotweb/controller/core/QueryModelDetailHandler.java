package com.agileai.hotweb.controller.core;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.QueryModelService;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class QueryModelDetailHandler extends BaseHandler{
	public QueryModelDetailHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		DataRow record = getService().findDetail(param);
		this.setAttributes(record);
		this.processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	protected QueryModelService getService() {
		return (QueryModelService)this.lookupService(this.getServiceId());
	}
}
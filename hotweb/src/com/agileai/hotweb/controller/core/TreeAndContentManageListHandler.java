package com.agileai.hotweb.controller.core;

import java.util.List;
import java.util.Map;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

abstract public class TreeAndContentManageListHandler extends BaseHandler {
	protected String rootColumnId = null;
    protected String defaultTabId = null;
    
 	protected String columnIdField = null;
 	protected String columnNameField = null;
	protected String columnParentIdField = null;
	protected String columnSortField = null;	
    
	public TreeAndContentManageListHandler() {
        super();
    }
	public ViewRenderer prepareDisplay(DataParam param){
		initParameters(param);
		this.setAttributes(param);
		String columnId = param.get("columnId",this.rootColumnId);
		this.setAttribute("columnId", columnId);
		this.setAttribute("isRootColumnId",String.valueOf(this.rootColumnId.equals(columnId)));
		
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		TreeModel filterTreeModel = null;
		TreeModel childModel = treeModel.getFullTreeMap().get(columnId);
		if (childModel != null){
			filterTreeModel = childModel;
		}else{
			filterTreeModel = treeModel;
		}
		
		String menuTreeSyntax = this.getTreeSyntax(treeModel,new StringBuffer());
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		String tabId = param.get(TreeAndContentManage.TAB_ID,this.defaultTabId);
		
		if (!TreeAndContentManage.BASE_TAB_ID.equals(tabId)){
			param.put("columnId",columnId);
			List<DataRow> rsList = getService().findContentRecords(filterTreeModel,tabId,param);
			this.setRsList(rsList);			
		}else{
			DataParam queryParam = new DataParam(columnIdField,columnId);
			DataRow row = getService().queryTreeRecord(queryParam);
			this.setAttributes(row);
		}
		
		this.setAttribute(TreeAndContentManage.TAB_ID, tabId);
		this.setAttribute(TreeAndContentManage.TAB_INDEX, getTabIndex(tabId));
		
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doDeleteTreeNodeAction(DataParam param){
		String rspText = SUCCESS;
		String columnId = param.get("columnId");
		TreeAndContentManage service = this.getService();
		List<DataRow> childRecords =  service.queryChildTreeRecords(columnId);
		if (!ListUtil.isNullOrEmpty(childRecords)){
			rspText = "hasChild";
			return new AjaxRenderer(rspText);
		}
		List<String> tabList = this.getTabList();
		for (int i=0;i < tabList.size();i++){
			String tabId = tabList.get(i);
			if (TreeAndContentManage.BASE_TAB_ID.equals(tabId))continue;
			List<DataRow> records = service.findContentRecords(null,tabId, new DataParam("columnId",columnId));
			if (!ListUtil.isNullOrEmpty(records)){
				rspText = "hasContent";
				return new AjaxRenderer(rspText);
			}			
		}
		service.deleteTreeRecord(columnId);
		return new AjaxRenderer(rspText);
	}
	
	public ViewRenderer doMoveTreeAction(DataParam param){
		String rspText = SUCCESS;
		String columnId = param.get("columnId");
		String targetParentId = param.get("targetParentId");
		TreeAndContentManage service = this.getService();
		int newMaxSortId = service.retrieveNewMaxSort(targetParentId);
		DataParam queryParam = new DataParam(columnIdField,columnId);
		DataRow row = service.queryTreeRecord(queryParam);
		row.put(this.columnSortField,newMaxSortId);
		row.put(this.columnParentIdField,targetParentId);
		service.updateTreeRecord(row.toDataParam(true));
		return new AjaxRenderer(rspText);
	}
	
	public ViewRenderer doMoveUpAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String columnId = param.get("columnId");
		if (service.isFirstTreeChild(columnId)){
			rspText = "isFirstNode";
		}else{
			service.changeTreeSort(columnId, true);			
		}
		return new AjaxRenderer(rspText);
	}
	public ViewRenderer doMoveDownAction(DataParam param){
		String rspText = SUCCESS;
		TreeAndContentManage service = this.getService();
		String columnId = param.get("columnId");
		if (service.isLastTreeChild(columnId)){
			rspText = "isLastNode";
		}else{
			service.changeTreeSort(columnId, false);
		}
		return new AjaxRenderer(rspText);
	}	
	
	protected String getTreeSyntax(TreeModel treeModel,StringBuffer treeSyntax){
    	String result = null;
    	try {
    		treeSyntax.append("<script type='text/javascript'>");
    		treeSyntax.append("d = new dTree('d');");
            String rootId = treeModel.getId();
            String rootName = treeModel.getName();
            treeSyntax.append("d.add('"+rootId+"',-1,'"+rootName+"',\"javascript:refreshContent('"+rootId+"')\");");
            treeSyntax.append("\r\n");
            buildTreeSyntax(treeSyntax,treeModel);
            treeSyntax.append("\r\n");
            treeSyntax.append("$('#treeArea').html(d.toString());");
            treeSyntax.append("\r\n");
            String currentColumnId = this.getAttributeValue("columnId");
            if (!StringUtil.isNullOrEmpty(currentColumnId)){
            	treeSyntax.append("d.s(d.getIndex('").append(currentColumnId).append("'));");            	
            }
            treeSyntax.append("\r\n");            	
            treeSyntax.append("</script>");
    		result = treeSyntax.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
    }
	
	protected void buildTreeSyntax(StringBuffer treeSyntax,TreeModel treeModel){
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
        for (int i=0;i < children.size();i++){
        	TreeModel child = children.get(i);
            String curNodeId = child.getId();
            String curNodeName = child.getName();
            if (!ListUtil.isNullOrEmpty(child.getChildren())){
            	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:refreshContent('"+curNodeId+"')\");");
            	treeSyntax.append("\r\n");
            }else{
            	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:refreshContent('"+curNodeId+"')\");");
            	treeSyntax.append("\r\n");
            }
            this.buildTreeSyntax(treeSyntax,child);
        }
    }	
	public ViewRenderer doQueryAction(DataParam param){
		return prepareDisplay(param);
	}

    public ViewRenderer doSaveTreeBaseRecordAction(DataParam param){
    	String rspText = SUCCESS;
    	getService().updateTreeRecord(param);
    	return new AjaxRenderer(rspText);
    }
    protected int getTabIndex(String curTabId) {
		int result = 0;
		List<String> temp = getTabList();
		for (int i=0;i < temp.size();i++){
			if (curTabId.equals(temp.get(i))){
				result = i;
				break;
			}
		}
		return result;
	}
    
    protected abstract List<String> getTabList();
    protected abstract TreeBuilder provideTreeBuilder(DataParam param);
    protected abstract void processPageAttributes(DataParam param);
    protected abstract void initParameters(DataParam param);
    
    
	public ViewRenderer doMoveContentAction(DataParam param){
		String rspText = SUCCESS;
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String columnId = param.get("curColumnId");
		String targetParentId = param.get("targetParentId");
		TreeAndContentManage service = this.getService();
		DataParam newParam = new DataParam();
		newParam.append(param);
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		newParam.put(colField,columnId);
		newParam.put("NEW_"+colField,targetParentId);
		service.updateTreeContentRelation(tabId, newParam);
		return new AjaxRenderer(rspText);
	}	
	public ViewRenderer doCopyContentAction(DataParam param){
		String rspText = SUCCESS;
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String targetParentId = param.get("targetParentId");
		TreeAndContentManage service = this.getService();
		DataParam newParam = new DataParam();
		newParam.append(param);
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		newParam.put(colField,targetParentId);
		service.insertTreeContentRelation(tabId, newParam);
		return new AjaxRenderer(rspText);
	}
	public ViewRenderer doIsLastRelationAction(DataParam param){
		String rspText = "false";
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		TreeAndContentManage service = this.getService();
		boolean isLast = service.isLastTreeContentRelation(tabId, param);
		if (isLast){
			rspText = "true";
		}
		return new AjaxRenderer(rspText);
	}
	public ViewRenderer doRemoveContentAction(DataParam param){
		TreeAndContentManage service = this.getService();
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String columnId = param.get("curColumnId");
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		param.put(colField,columnId);
		service.removeTreeContentRelation(tabId, param);
		return prepareDisplay(param);
	}
	public ViewRenderer doDeleteAction(DataParam param){
		TreeAndContentManage service = this.getService();
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String columnId = param.get("curColumnId");
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		param.put(colField,columnId);
		service.deletContentRecord(tabId,param);	
		return prepareDisplay(param);
	}
    
    
    protected TreeAndContentManage getService() {
        return (TreeAndContentManage) this.lookupService(this.getServiceId());
    }    
}

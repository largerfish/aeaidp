package com.agileai.hotweb.common;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.agileai.util.XmlUtil;

public class ModuleManager {
	private ClassLoader appClassLoader = null;
	public static HashMap<ClassLoader,ModuleManager> ModulerCache = new HashMap<ClassLoader,ModuleManager>();
	private HashMap<String,String> serviceIndexs = null;
	private HashMap<String,String> handlerIndexs = null;
	private HashMap<String,String> portletIndexs = null;
	
	private ModuleManager(ClassLoader appClassLoader){
		this.appClassLoader = appClassLoader;
	}
	
	public static synchronized ModuleManager getOnly(ClassLoader appClassLoader){
		ModuleManager result = null;
		if (!ModulerCache.containsKey(appClassLoader)){
			ModuleManager moduleManager = new ModuleManager(appClassLoader);
			ModulerCache.put(appClassLoader, moduleManager);
			moduleManager.serviceIndexs = new HashMap<String,String>();
			moduleManager.handlerIndexs = new HashMap<String,String>();
			moduleManager.portletIndexs = new LinkedHashMap<String,String>();
		}
		result = ModulerCache.get(appClassLoader);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public void initIndexs(){
		try {
			if (this.appClassLoader.getResource("HandlerContext.xml") != null){
				String webInfPath = getWebInfPath("HandlerContext.xml");
				
				File handlersIndexFile = new File(webInfPath+File.separator+"HandlerIndex.xml");
				if (handlersIndexFile.exists()){
					Document handlersIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"HandlerIndex.xml");
					String handlerXPath = "//Handlers/bean";
					List<Node> handlerNodes = handlersIndexDom.selectNodes(handlerXPath);
					if (handlerNodes != null){
						for (int i=0;i < handlerNodes.size();i++){
							Element element = (Element)handlerNodes.get(i);
							String id = element.attributeValue("id");
							String module = element.attributeValue("module");
							this.handlerIndexs.put(id, module);
						}
					}				
				}
				
				File servicesIndexFile = new File(webInfPath+File.separator+"ServiceIndex.xml");
				if (servicesIndexFile.exists()){
					Document servicesIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"ServiceIndex.xml");
					String serviceXPath = "//Services/bean";
					List<Node> serviceNodes = servicesIndexDom.selectNodes(serviceXPath);
					if (serviceNodes != null){
						for (int i=0;i < serviceNodes.size();i++){
							Element element = (Element)serviceNodes.get(i);
							String id = element.attributeValue("id");
							String module = element.attributeValue("module");
							this.serviceIndexs.put(id, module);
						}
					}				
				}
				
				File portletIndexFile = new File(webInfPath+File.separator+"PortletIndex.xml");
				if (portletIndexFile.exists()){
					Document portletIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"PortletIndex.xml");
					String serviceXPath = "//Portlets/bean";
					List<Node> portletNodes = portletIndexDom.selectNodes(serviceXPath);
					if (portletNodes != null){
						for (int i=0;i < portletNodes.size();i++){
							Element element = (Element)portletNodes.get(i);
							String id = element.attributeValue("id");
							String module = element.attributeValue("module");
							this.portletIndexs.put(id, module);
						}
					}				
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getWebInfPath(String fileName) throws URISyntaxException{
		URI portletURI = this.appClassLoader.getResource(fileName).toURI();
		File porltetContextFile = new File(portletURI);
		String webInfPath = porltetContextFile.getParentFile().getParentFile().getAbsolutePath();
		return webInfPath;
	}
	
	public String getServiceModule(String serviceId){
		if (!serviceIndexs.containsKey(serviceId)){
			try {
				URI handlerURI = this.appClassLoader.getResource("HandlerContext.xml").toURI();
				File handlerContextFile = new File(handlerURI);
				String webInfPath = handlerContextFile.getParentFile().getParentFile().getAbsolutePath();
				
				File servicesIndexFile = new File(webInfPath+File.separator+"ServiceIndex.xml");
				if (servicesIndexFile.exists()){
					Document servicesIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"ServiceIndex.xml");
					String serviceXPath = "//Services/bean[@id='"+serviceId+"']";
					Node serviceNode = servicesIndexDom.selectSingleNode(serviceXPath);
					if (serviceNode != null){
						Element element = (Element)serviceNode;
						String id = element.attributeValue("id");
						String module = element.attributeValue("module");
						this.serviceIndexs.put(id, module);
					}		
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return serviceIndexs.get(serviceId);
	}
	
	public String getHandlerModule(String handlerId){
		if (!handlerIndexs.containsKey(handlerId)){
			try {
				URI handlerURI = this.appClassLoader.getResource("HandlerContext.xml").toURI();
				File handlerContextFile = new File(handlerURI);
				String webInfPath = handlerContextFile.getParentFile().getParentFile().getAbsolutePath();
				
				File handlersIndexFile = new File(webInfPath+File.separator+"HandlerIndex.xml");
				if (handlersIndexFile.exists()){
					Document handlersIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"HandlerIndex.xml");
					String handlerXPath = "//Handlers/bean[@id='"+handlerId+"']";
					Node handlerNode = handlersIndexDom.selectSingleNode(handlerXPath);
					if (handlerNode != null){
						Element element = (Element)handlerNode;
						String id = element.attributeValue("id");
						String module = element.attributeValue("module");
						this.handlerIndexs.put(id, module);
					}				
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return handlerIndexs.get(handlerId);
	}
	
	public String getPortletModule(String portletId){
		if (!portletIndexs.containsKey(portletId)){
			try {
				URI handlerURI = this.appClassLoader.getResource("HandlerContext.xml").toURI();
				File handlerContextFile = new File(handlerURI);
				String webInfPath = handlerContextFile.getParentFile().getParentFile().getAbsolutePath();
				
				File handlersIndexFile = new File(webInfPath+File.separator+"PortletIndex.xml");
				if (handlersIndexFile.exists()){
					Document portletsIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"PortletIndex.xml");
					String handlerXPath = "//Portlets/bean[@id='"+portletId+"']";
					Node handlerNode = portletsIndexDom.selectSingleNode(handlerXPath);
					if (handlerNode != null){
						Element element = (Element)handlerNode;
						String id = element.attributeValue("id");
						String module = element.attributeValue("module");
						this.portletIndexs.put(id, module);
					}				
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return portletIndexs.get(portletId);
	}	

	@SuppressWarnings("unchecked")
	public List<String> getPortletIdList(){
		List<String> result = new ArrayList<String>();
		try {
			if (this.appClassLoader.getResource("HandlerContext.xml") != null){
				String webInfPath = getWebInfPath("HandlerContext.xml");
				File portletIndexFile = new File(webInfPath+File.separator+"PortletIndex.xml");
				if (portletIndexFile.exists()){
					Document portletIndexDom = XmlUtil.readDocument(webInfPath+File.separator+"PortletIndex.xml");
					String serviceXPath = "//Portlets/bean";
					List<Node> portletNodes = portletIndexDom.selectNodes(serviceXPath);
					if (portletNodes != null){
						for (int i=0;i < portletNodes.size();i++){
							Element element = (Element)portletNodes.get(i);
							String id = element.attributeValue("id");
							String module = element.attributeValue("module");
							this.portletIndexs.put(id, module);
						}
					}				
				}	
			}
			Iterator<String> iter = this.portletIndexs.keySet().iterator();
			while(iter.hasNext()){
				String portletId = iter.next();
				result.add(portletId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public boolean load(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		classLoaderFactory.createModuleClassLoader(moduleName);
		result = true;
		return result;
	}
	
	public boolean unload(String moduleName){
		boolean result = false;
		ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
		ClassLoader moduleClassLoader = classLoaderFactory.rejectModuleClassLoader(moduleName);
		moduleClassLoader = null;
		System.gc();
		result = true;
		return result;
	}
	
	public boolean reload(String moduleName){
		boolean result = false;
		
		result = this.unload(moduleName);
		if (result){
			result = this.load(moduleName);			
		}
		return result;
	}	
}

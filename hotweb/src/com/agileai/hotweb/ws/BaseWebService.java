package com.agileai.hotweb.ws;

import javax.sql.DataSource;

import com.agileai.common.AppConfig;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.util.StringUtil;

public class BaseWebService {
	protected DataSource getDataSource(){
		return BeanFactory.instance().getDataSource();
	}
	protected DataSource getDataSource(String dataSourceId){
		return BeanFactory.instance().getDataSource(dataSourceId);
	}
	public AppConfig getAppConfig(){
		AppConfig result = (AppConfig)BeanFactory.instance().getAppConfig();
		return result;
	}		
	protected Object lookupService(String serviceId){
		Object service = null;
		ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
		ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
		String moduleName = moduleManager.getServiceModule(serviceId);
		if (StringUtil.isNotNullNotEmpty(moduleName)){
			ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
			ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
			service = BeanFactory.instance(moduleClassLoader).getBean(serviceId);
		}else{
			service = BeanFactory.instance().getBean(serviceId);
		}
		return service;
	}
	protected <ServiceType> ServiceType lookupService(Class<ServiceType> serviceClass){
		String serviceId = BaseHandler.buildServiceId(serviceClass);
		Object service = this.lookupService(serviceId);
		return serviceClass.cast(service);
	}
}

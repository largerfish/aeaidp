package com.agileai.hotweb.i18n;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.agileai.hotweb.common.ModuleClassLoader;

public class BundleContext {
	public static final String I18NPropertiesName = "i18n/resource/Locale_";
	
	private static BundleContext bundleContext = null;
	private static HashMap<ClassLoader,ResourceBundle> ResourceBundleCache = new HashMap<ClassLoader,ResourceBundle>();
	
	private BundleContext(){
	}
	
	public static synchronized BundleContext getOnly(){
		if (bundleContext == null){
			bundleContext = new BundleContext();
		}
		return bundleContext;
	}
	
	public synchronized void init(List<String> localeExtNameList){
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ResourceBundle resourceBundle = new ResourceBundle();
		ResourceBundleCache.put(classLoader, resourceBundle);
		
		for (int i=0;i < localeExtNameList.size();i++){
			String localeExtName = localeExtNameList.get(i);
			String propertiesFileName = I18NPropertiesName + localeExtName + ".properties";
			InputStream inputStream = classLoader.getResourceAsStream(propertiesFileName);
			try {
				Properties prop = new Properties();
				prop.load(inputStream);
				if (inputStream != null) {
					inputStream.close();
				}
				resourceBundle.PropertiesCache.put(localeExtName, prop);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public ResourceBundle getResourceBundle(){
		ResourceBundle result = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		ClassLoader appClassLoader = null;
		if (classLoader instanceof ModuleClassLoader){
			ModuleClassLoader moduleClassLoader = (ModuleClassLoader)classLoader;
			appClassLoader = moduleClassLoader.getParent();
		}else{
			appClassLoader = classLoader;
		}
		result = ResourceBundleCache.get(appClassLoader);
		return result;
	}
}
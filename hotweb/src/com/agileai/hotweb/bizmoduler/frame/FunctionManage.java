package com.agileai.hotweb.bizmoduler.frame;

import java.util.List;

import com.agileai.hotweb.domain.system.FuncHandler;
import com.agileai.hotweb.domain.system.FuncMenu;
import com.agileai.hotweb.domain.system.Operation;

public interface FunctionManage{
	public FuncMenu getFunction(String functionId);
	public List<FuncMenu> getFuncMenuList();

	public FuncHandler getFuncHandler(String handlerId);
	public Operation getOperation(String operationId);
	
	public void clearFuncTreeCache();
}

package com.agileai.hotweb.bizmoduler.core;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataParamConvert;
import com.agileai.domain.db.Column;
import com.agileai.domain.db.Table;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.common.ClassLoaderFactory;
import com.agileai.hotweb.common.DaoHelper;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.util.DBUtil;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class BaseService {
	protected Map<String,Table> tableMetaDataContainer = new HashMap<String,Table>();
	protected String tableName = null;
	protected String sqlNameSpace = null;
	protected DaoHelper daoHelper = null;
	protected String uniqueField = null;
	protected HashMap<String,DataParamConvert> fieldConvertMap = new HashMap<String, DataParamConvert>();
	
	protected ClassLoader classLoader = null;
	
	protected DataSource getDataSource(){
		return BeanFactory.instance().getDataSource();
	}
	protected DataSource getDataSource(String dataSourceId){
		return BeanFactory.instance().getDataSource(dataSourceId);
	}
	
	public DaoHelper getDaoHelper() {
		return daoHelper;
	}
	public void setDaoHelper(DaoHelper daoHelper) {
		this.daoHelper = daoHelper;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getSqlNameSpace() {
		return sqlNameSpace;
	}
	public void setSqlNameSpace(String sqlNameSpace) {
		this.sqlNameSpace = sqlNameSpace;
	}
	public String getUniqueField() {
		return uniqueField;
	}
	public void setUniqueField(String uniqueField) {
		this.uniqueField = uniqueField;
	}
	protected Table getTableMetaData(String tableName){
		Table table = null;
		if (!tableMetaDataContainer.containsKey(tableName)){
			try {
				DataSource dataSource = BeanFactory.instance().getDataSource();
				Column[] columns = DBUtil.getColumns(tableName,dataSource);
				if (columns != null && columns.length > 0){
					int count = columns.length;
					Table temp = new Table(tableName);
					for (int i=0;i < count;i++){
						Column column = columns[i];
						temp.addColumn(column);
					}
					tableMetaDataContainer.put(tableName,temp);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		table = tableMetaDataContainer.get(tableName);
		return table;
	}
	protected void processDataType(DataParam dataParam,String tableName){
		Table table = getTableMetaData(tableName);
		List<Column> dateColumns = table.getDateTypeColumns();
		int count = dateColumns.size();
		for (int i=0;i < count;i++){
			Column column = dateColumns.get(i);
			String columnName = column.getName();
			Object paramValue = dataParam.getObject(columnName);
			if (paramValue != null && paramValue instanceof String){
				if (!String.valueOf(paramValue).trim().equals("") && !String.valueOf(paramValue).trim().equals("null")){
					Date dateValue = DateUtil.getDateTime((String)paramValue);
					dataParam.put(columnName,dateValue);					
				}else{
					dataParam.remove(columnName);
				}
			}
		}
		if (!this.fieldConvertMap.isEmpty()){
			Iterator<String> keys = this.fieldConvertMap.keySet().iterator();
			while (keys.hasNext()){
				String key = keys.next();
				Column column = table.getColumn(key);
				if (column != null){
					DataParamConvert convert = this.fieldConvertMap.get(key);
					Object value = dataParam.get(key);
					Object convertValue = convert.convert(value, column);
					dataParam.put(key,convertValue);
				}
			}
		}
	}

	protected Object lookupService(String serviceId){
		Object service = null;
		if (classLoader != null){
			ClassLoader appClassLoader = this.classLoader.getParent();
			ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
			String moduleName = moduleManager.getServiceModule(serviceId);
			if (StringUtil.isNotNullNotEmpty(moduleName)){
				ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
				ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
				service = BeanFactory.instance(moduleClassLoader).getBean(serviceId);
			}else{
				service = BeanFactory.instance().getBean(serviceId);
			}
		}else{
			ClassLoader appClassLoader = Thread.currentThread().getContextClassLoader();
			ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
			String moduleName = moduleManager.getServiceModule(serviceId);
			if (StringUtil.isNotNullNotEmpty(moduleName)){
				ClassLoaderFactory classLoaderFactory = ClassLoaderFactory.instance(appClassLoader);
				ClassLoader moduleClassLoader = classLoaderFactory.createModuleClassLoader(moduleName);
				service = BeanFactory.instance(moduleClassLoader).getBean(serviceId);
			}else{
				service = BeanFactory.instance().getBean(serviceId);
			}
		}
		return service;
	}
	protected <ServiceType> ServiceType lookupService(Class<ServiceType> serviceClass){
		String serviceId = BaseHandler.buildServiceId(serviceClass);
		Object service = this.lookupService(serviceId);
		return serviceClass.cast(service);
	}
	public HashMap<String, DataParamConvert> getFieldConvertMap() {
		return fieldConvertMap;
	}
	public void setFieldConvertMap(HashMap<String, DataParamConvert> fieldConvertMap) {
		this.fieldConvertMap = fieldConvertMap;
	}
	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}
	public AppConfig getAppConfig(){
		AppConfig result = (AppConfig)BeanFactory.instance().getAppConfig();
		return result;
	}
}

package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.db.Column;
import com.agileai.domain.db.Table;
import com.agileai.hotweb.common.Constants;

public class StandardServiceImpl extends BaseService implements StandardService {
	private String primaryKey = null;
	private String pkGenPolicy = null;
	
	public StandardServiceImpl(){
	}
	public void createRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertRecord";
		processDataType(param, tableName);
		processPrimaryKeys(param);
		this.daoHelper.insertRecord(statementId, param);
	}
	public void createRecord(DataParam param, String pKeyValue) {
		String statementId = sqlNameSpace+"."+"insertRecord";
		processDataType(param, tableName);
		param.put(primaryKey,pKeyValue);
		this.daoHelper.insertRecord(statementId, param);
	}	
	public DataRow getRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	public void updateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
	}
	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
	public List<DataRow> findRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
	protected void processPrimaryKeys(DataParam param){
		KeyGenerator keyGenerator = new KeyGenerator();
		if (Constants.PKType.INCREASE.equals(pkGenPolicy)){
			param.put(primaryKey,keyGenerator.getMaxId(tableName, primaryKey,getDataSource()));
		}
		else if(Constants.PKType.CHARGEN.equals(pkGenPolicy)){
			String genPk = keyGenerator.genKey();
			param.put(primaryKey,genPk);
		}
	}
	public String getPrimaryKey() {
		if (this.primaryKey == null){
			Table table = getTableMetaData(tableName);
			List<Column> columns = table.getColumns();			
			int count = columns.size();
			for (int i=0;i < count;i++){
				Column column = columns.get(i);
				if (column.isPK()){
					this.primaryKey = column.getName();
					break;
				}
			}
		}
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getPkGenPolicy() {
		return pkGenPolicy;
	}
	public void setPkGenPolicy(String pkGenPolicy) {
		this.pkGenPolicy = pkGenPolicy;
	}
	protected void processDataType(DataParam pageParam,String tableName){
		super.processDataType(pageParam, tableName);
		Table table = getTableMetaData(tableName);
		List<Column> columns = table.getColumns();
		if (this.primaryKey == null){
			int count = columns.size();
			for (int i=0;i < count;i++){
				Column column = columns.get(i);
				if (column.isPK()){
					this.primaryKey = column.getName();
					break;
				}
			}
		}
		if (this.pkGenPolicy == null){
			int count = columns.size();
			for (int i=0;i < count;i++){
				Column column = columns.get(i);
				if (this.primaryKey != null
						&& this.primaryKey.equals(column.getName())){
					if ("java.lang.Integer".equals(column.getClassName())
							|| "java.lang.Long".equals(column.getClassName())
							|| "java.math.BigDecimal".equals(column.getClassName())
							|| "java.math.BigInteger".equals(column.getClassName())){
						this.pkGenPolicy = Constants.PKType.INCREASE;
						break;
					}
					else if ("java.lang.String".equals(column.getClassName())){
						if (column.getLength()%36 == 0 && column.getLength() < 150){
							this.pkGenPolicy = Constants.PKType.CHARGEN;
							break;
						}
						else{
							this.pkGenPolicy = Constants.PKType.ASSIGN;
							break;
						}
					}
				}
			}
		}
	}
}

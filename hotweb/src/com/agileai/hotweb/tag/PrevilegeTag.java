package com.agileai.hotweb.tag;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.agileai.hotweb.common.HotwebAuthHelper;
import com.agileai.hotweb.domain.PageBean;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.domain.system.FuncHandler;
import com.agileai.hotweb.domain.system.FuncMenu;
import com.agileai.hotweb.domain.system.Operation;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class PrevilegeTag extends TagSupport {
	private static final long serialVersionUID = -6358576256883284180L;
	private String code;  
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int doStartTag() throws JspException {
		boolean valid = false;
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		PageBean pageBean = (PageBean)request.getAttribute(ViewRenderer.PAGE_BEAN_KEY);
		if (pageBean != null){
			HttpSession session = request.getSession(false);
			if (session != null){
				Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY);
				if (profile != null){
					User user = (User)profile.getUser();
					if (user != null){
						HotwebAuthHelper authHelper = new HotwebAuthHelper(user);
						String currentFuncId = authHelper.getCurrentFuncId();
						if (!StringUtil.isNullOrEmpty(currentFuncId)){
							FuncMenu function = authHelper.getFunction(currentFuncId);
							if (function != null){
								String handlerId = pageBean.getHandlerId();
								List<FuncHandler> funcHandlers = function.getHandlers();
								FuncHandler funcHandler = this.getHandler(funcHandlers, handlerId);
								if (funcHandler != null){
									List<Operation> operationList = funcHandler.getOperations();
									boolean matched = false;
									if (!operationList.isEmpty()){
										for (int i=0;i < operationList.size();i++){
											Operation operation = operationList.get(i);
											String operCode = operation.getOperCode();
											String operationId = operation.getOperId();
											if (code.equals(operCode)){
												matched = true;
												if (user.isAdmin() || user.containResouce(Resource.Type.Operation,operationId)){
													valid = true;
													break;
												}
											}
										}
									}
									if (!matched){
										valid = true;
									}
								}
							}							
						}
					}
				}				
			}
		}
		if (valid){
			return EVAL_BODY_INCLUDE;
		}else{
			return SKIP_BODY;			
		}
	}
	
	private FuncHandler getHandler(List<FuncHandler> funcHandlers,String handlerCode){
		FuncHandler result = null;
		for (int i=0;i < funcHandlers.size();i++){
			FuncHandler funcHandler = (FuncHandler)funcHandlers.get(i);
			if (handlerCode.equals(funcHandler.getHandlerCode())){
				result = funcHandler;
				break;
			}
		}
		return result;
	}
}

package com.agileai.miscdp.server;

import java.util.List;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapHeader;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.DOMUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ClientSoapHeader extends AbstractSoapInterceptor {

	public static final String xml_namespaceUR_att = "http://service.hotserver.agileai.com/authentication";
	public static final String xml_authentication_el = "authentication";
	public static final String xml_userID_el = "userId";
	public static final String xml_password_el = "password";
	
	private String userId = null;
	private String password = null;
	
	public ClientSoapHeader(String userId,String password) {
		super(Phase.WRITE);
		this.userId = userId;
		this.password = password;
	}
	
	public void handleMessage(SoapMessage message) throws Fault {
		QName qname = new QName("RequestSOAPHeader");

		Document doc = (Document) DOMUtils.createDocument();
		Element root = doc.createElementNS(xml_namespaceUR_att,xml_authentication_el);
		Element eUserId = doc.createElement(xml_userID_el);
		eUserId.setTextContent(userId);
		Element ePwd = doc.createElement(xml_password_el);
		ePwd.setTextContent(password);
		
		root.appendChild(eUserId);
		root.appendChild(ePwd);
//		XMLUtils.printDOM(root);
		SoapHeader head = new SoapHeader(qname, root);
		List<Header> headers = message.getHeaders();
		headers.add(head);
	}
}
﻿package com.agileai.miscdp.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
/**
 * 插件变量辅助类
 */
public class VariableUtil {
	
	public static void addEclipseVariable(String name, String value, String description) {
		IStringVariableManager stringVariableManager = VariablesPlugin.getDefault().getStringVariableManager();
		//IStringVariableManager stringVariableManager = StringVariableManager.getDefault();
	    
	    IValueVariable newVariable = stringVariableManager.newValueVariable(name, description);
		newVariable.setValue(value);
	    try {
	    	stringVariableManager.removeVariables(new IValueVariable[] {newVariable});
	    	stringVariableManager.addVariables(new IValueVariable[] {newVariable});
		} catch (CoreException e1) {
			e1.printStackTrace();
		}
	}
	public static void removeEclipseVariable(String name) {
		IStringVariableManager stringVariableManager = VariablesPlugin.getDefault().getStringVariableManager();
	    
		IValueVariable variable = stringVariableManager.getValueVariable(name);
		if (variable != null) {
		    stringVariableManager.removeVariables(new IValueVariable[] {variable});	
		}
	}
	
	public static IValueVariable getEclipseVariable(String name) {
		IStringVariableManager stringVariableManager = VariablesPlugin.getDefault().getStringVariableManager();
		//IStringVariableManager stringVariableManager = StringVariableManager.getDefault();

		IValueVariable variable = stringVariableManager.getValueVariable(name);
		return variable;
		
//	    IValueVariable[] elements = stringVariableManager.getValueVariables();
//	    for (IValueVariable variable : elements) {
//	    	if (variable.getName().equals(name)) {
//	    		return variable;
//	    	}
//	    }
//	    return null;
	}
}

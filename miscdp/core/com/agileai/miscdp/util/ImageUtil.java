﻿package com.agileai.miscdp.util;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

import com.agileai.miscdp.DeveloperConst;
/**
 * 图标操作辅助类
 */
public class ImageUtil {
	private static ImageRegistry register = new ImageRegistry();
	@SuppressWarnings("rawtypes")
	private static Set keys = new HashSet();
	static {
		initial();
	}

	public static ImageDescriptor getDescriptor(String key) {
		ImageDescriptor image = register.getDescriptor(key);
		if (image == null) {
			image = ImageDescriptor.getMissingImageDescriptor();
		}
		return image;
	}

	public static Image get(String key) {
		Image image = register.get(key);
		if (image == null) {
			image = ImageDescriptor.getMissingImageDescriptor().createImage();
		}
		return image;
	}

	@SuppressWarnings("unchecked")
	public static String[] getImageKey() {
		return (String[]) keys.toArray(new String[keys.size()]);
	}

	@SuppressWarnings("unchecked")
	private static void initial() {
		Bundle bundle = Platform.getBundle(DeveloperConst.PLUGIN_ID);
		URL url = bundle.getEntry("icons");
		try {
			url = FileLocator.toFileURL(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		File file = new File(url.getPath());
		File[] images = file.listFiles();
		for (int i = 0; i < images.length; i++) {
			File f = images[i];
			if (!f.isFile()) {
				continue;
			}
			String name = f.getName();
			if (!name.endsWith(".gif")) {
				continue;
			}
			String key = name.substring(0, name.indexOf('.'));
			URL fullPathString = bundle.getEntry("icons/" + name);
			ImageDescriptor des = ImageDescriptor.createFromURL(fullPathString);
			register.put(key, des);
			keys.add(key);
		}
	}
}

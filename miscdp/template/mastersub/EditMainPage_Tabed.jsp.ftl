<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/mastersub",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro MasterSubFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local editMainView = .node.EditMainView>
<#local masterEditArea = .node.EditMainView.MasterEditArea>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page language="java" import="java.util.*" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String currentSubTableId = pageBean.getStringValue("currentSubTableId");
String currentSubTableIndex = pageBean.getStringValue("currentSubTableIndex");
%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@detailTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveMasterRecord(){
	if (validate()){
		if (ele("currentSubTableId")){
			var subTableId = $("#currentSubTableId").val();
			if (!checkEntryRecords(subTableId)){
				return;
			}
		}
		showSplash();
<#if (checkUnique)>		
		postRequest('form1',{actionType:'checkUnique',onComplete:function(responseText){
			if (responseText == ''){
				postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
					if ("fail" != responseText){
						$('#operaType').val('update');
						$('#${baseInfo.@tablePK}').val(responseText);
						doSubmit({actionType:'prepareDisplay'});
					}else{
						hideSplash();
						writeErrorMsg('保存操作出错啦！');
					}
				}});
			}else{
				hideSplash();
				writeErrorMsg(responseText);			
			}
		}});
<#else>		
		postRequest('form1',{actionType:'saveMasterRecord',onComplete:function(responseText){
			if ("fail" != responseText){
				$('#operaType').val('update');
				$('#${baseInfo.@tablePK}').val(responseText);
				doSubmit({actionType:'prepareDisplay'});
			}else{
				hideSplash();
				writeErrorMsg('保存操作出错啦！');
			}
		}});
</#if>
	}
}
function changeSubTable(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'changeSubTable'});
}
function refreshPage(){
	doSubmit({actionType:'changeSubTable'});
}
function addEntryRecord(subTableId){
	$('#currentSubTableId').val(subTableId);
	doSubmit({actionType:'addEntryRecord'});
}
function deleteEntryRecord(subTableId){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		$('#currentSubTableId').val(subTableId);
		doSubmit({actionType:'deleteEntryRecord'});	
	}
}
function checkEntryRecords(subTableId){
	var result = true;
	var currentRecordSize = $('#currentRecordSize').val();
<#list editMainView.* as c>
<#if c?node_type = 'element' && c?node_name = 'SubEntryEditArea' >
	if ("${c.@subTableId}"==subTableId){
		for (var i=0;i < currentRecordSize;i++){
			<@Form.RowSimpleValidation paramArea=c></@Form.RowSimpleValidation>	
		}
	}
 </#if>
</#list>	
	return result;
}
var insertSubRecordBox;
function insertSubRecordRequest(title,handlerId){
	if (!insertSubRecordBox){
		insertSubRecordBox = new PopupBox('insertSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=insert&${baseInfo.@tablePK}='+$('#${baseInfo.@tablePK}').val();
	insertSubRecordBox.sendRequest(url);	
}
var copySubRecordBox;
function copySubRecordRequest(title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!copySubRecordBox){
		copySubRecordBox = new PopupBox('copySubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType=copy&'+subPKField+'='+$("#"+subPKField).val();
	copySubRecordBox.sendRequest(url);	
}
var viewSubRecordBox;
function viewSubRecordRequest(operaType,title,handlerId,subPKField){
	clearSelection();
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (!viewSubRecordBox){
		viewSubRecordBox = new PopupBox('viewSubRecordBox',title,{size:'normal',height:'300px',top:'10px'});
	}
	var url = 'index?'+handlerId+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	viewSubRecordBox.sendRequest(url);
}
function deleteSubRecord(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if (confirm('确认要删除该条记录吗？')){
		doSubmit({actionType:'deleteSubRecord'});	
	}
}
function doMoveUp(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	if (!isSelectedRow()){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	doSubmit({actionType:'moveDown'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top:7px;">
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div class="photobg1" id="tabHeader">
<div class="newarticle1" onclick="changeSubTable('_base')">基础信息</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<#list editMainView.* as c>
 <#if c?node_type = 'element' &&  (c?node_name = 'SubEntryEditArea' || c?node_name = 'SubListViewArea') >
 <div class="newarticle1" onclick="changeSubTable('${c.@subTableId}')">${c.@tabName}</div>
 </#if>
</#list>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:auto;">
<div style="margin:2px;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveMasterRecord();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<@Form.DetailEditArea paramArea=masterEditArea></@Form.DetailEditArea>
</table>
</div>
</div>
<%if (!"insert".equals(pageBean.getOperaType())){%>
<#list editMainView.* as c>
<#if c?node_type = 'element' && c?node_name = 'SubEntryEditArea' >
<%if ("${c.@subTableId}".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer1" style="height:auto;">    
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="addEntryRecord('${c.@subTableId}')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="deleteEntryRecord('${c.@subTableId}')"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="refreshPage()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" />取消</td>
   <#if (Util.isValidValue(c.@sortField)) >
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()" class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>           
	</#if>  
</tr>   
   </table>
</div>
<div style="margin:2px;">
<table border="0" cellpadding="0" cellspacing="0" class="dataTable ecSide" id="dataTable">
<thead>
  <tr>
    <th width="80" align="center" nowrap="nowrap">序号</th>
	<#list c["fa:FormObject"] as formObject><@EditFormHeadCell formObject=formObject/></#list>
  </tr>
</thead>
<tbody>
<%
List paramRecords = (List)pageBean.getAttribute("${c.@subTableId}Records");
pageBean.setRsList(paramRecords);
int paramSize = pageBean.listSize();
for (int i=0;i < paramSize;i++){
%>
  <tr onmouseout="ECSideUtil.unlightRow(this);" onmouseover="ECSideUtil.lightRow(this);" onclick="ECSideUtil.selectRow(this,'form1');selectRow(this,{currentRecordIndex:'<%=i%>'})">
	<td style="text-align:center"><%=i+1%>
	<@Form.RowEditHiddenArea hiddenArea=c></@Form.RowEditHiddenArea>
	<input id="state_<%=i%>" name="state_<%=i%>" type="hidden" value="<%=pageBean.inputValue(i,"_state")%>" />
	</td>
	<#list c["fa:FormObject"] as formObject><@EditFormBodyCell formObject=formObject/></#list>
  </tr>
<%}%>
</tbody>  
</table>
</div>
</div>
<input type="hidden" id="currentRecordSize" name="currentRecordSize" value="<%=pageBean.listSize()%>" />
<input type="hidden" id="currentRecordIndex" name="currentRecordIndex" value="" />
<script language="javascript">
<%for (int i=0;i < paramSize;i++){%>
	$("input[id$='_<%=i%>'],select[id$='_<%=i%>']").change(function(){
		if ($("#state_<%=i%>").val()==""){
			$("#state_<%=i%>").val('update');
		}
	});
<%}%>
setRsIdTag('currentRecordIndex');
<#if Form.existDateField(c)>
for (var i=0;i < $('#currentRecordSize').val();i++){
	<@Form.RowInitCalendar paramArea=c></@Form.RowInitCalendar>	
}
</#if>
</script>
<%}%>
<#elseif c?node_type = 'element' && c?node_name = 'SubListViewArea'>
<%if ("${c.@subTableId}".equals(currentSubTableId)){ %>
<div class="photobox newarticlebox" id="Layer2" style="height:auto;">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="insertSubRecordRequest('${c.@tabName}','${c.@handlerId}')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="copySubRecordRequest('${c.@tabName}','${c.@handlerId}','${c.@primaryKey}')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="viewSubRecordRequest('update','${c.@tabName}','${c.@handlerId}','${c.@primaryKey}')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>      
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="deleteSubRecord()"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
<#if (Util.isValidValue(c.@sortField)) >
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()" class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()" class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>           
</#if>         
</tr>   
   </table>
</div>
<div style="margin:2px;">
<%
List param1Records = (List)pageBean.getAttribute("${c.@subTableId}Records");
pageBean.setRsList(param1Records);
%>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="${c.@tabName}.csv"
retrieveRowsCallback="process" xlsFileName="${c.@tabName}.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize|export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto"
>
<#local row =c["fa:Row"][0]>
<ec:row styleClass="odd" ondblclick="viewSubRecordRequest('detail','${c.@tabName}','${c.@handlerId}','${c.@primaryKey}')" onclick="selectRow(this,${Util.parseRsIdField(row.@rsIdColumn)})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${r"${GLOBALROWCOUNT}"}" />
	<#list row["fa:Column"] as columnVar>
	<@Form.Column column=columnVar></@Form.Column>
	</#list>
</ec:row>
</ec:table>
</div>
</div>
<input type="hidden" name="${c.@primaryKey}" id="${c.@primaryKey}" value=""/>
<script language="javascript">
setRsIdTag('${c.@primaryKey}');
</script>
<%}%>

 </#if>
</#list>

<input type="hidden" id="currentSubTableId" name="currentSubTableId" value="<%=pageBean.inputValue("currentSubTableId")%>" />
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
<script language="javascript">
$("#Layer0").hide();
</script>
<%}%>
<%}%>
<script language="javascript">
new Tab('tab','tabHeader','Layer',<%=currentSubTableIndex%>);
<%if (!"_base".equals(pageBean.inputValue("currentSubTableId"))){%>
$("#Layer0").hide();
<%}%>
</script>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<@Form.DetailHiddenArea hiddenArea=masterEditArea></@Form.DetailHiddenArea>
</div>
</form>
<script language="javascript">
<@Form.Validation paramArea=masterEditArea></@Form.Validation>
initDetailOpertionImage();
$(function(){
	resetTabHeight(80);
});
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>

<#macro EditFormHeadCell formObject>
<#local formAtom = formObject>
<#local type=formAtom.*[0]?node_name?lower_case>
<#if type !="hidden">
	<th width="100" align="center">${formObject.@label}</th>
</#if>
</#macro>

<#macro EditFormBodyCell formObject>
<#local formAtom = formObject>
<#local type=formAtom.*[0]?node_name?lower_case>
<#if type !="hidden">
	<td><@Form.RowEditFormAtom formAtom=formObject/></td>
</#if>
</#macro>
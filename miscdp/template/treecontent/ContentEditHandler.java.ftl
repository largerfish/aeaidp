<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
<#local contentTableInfo = baseInfo.ContentTableInfo[contentIndex]>
<#local editView = .node.ContentEditView[contentIndex]>
<#local detailArea = .node.ContentEditView[contentIndex].DetailEditArea>
package ${Util.parsePkg(editView.@handlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.domain.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(editView.@handlerClass)} extends TreeAndContentManageEditHandler{
	public ${Util.parseClass(editView.@handlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
        this.tabId = "${editView.@tabId}";
        this.columnIdField = "${baseInfo.@columnIdField}";
        this.contentIdField = "${contentTableInfo.@primaryKey}";
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(detailArea)><#list detailArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
		
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>
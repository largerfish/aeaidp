<#ftl ns_prefixes={"fa":"http://www.hotweb.agileai.com/model"}>
<#include "/common/Util.ftl">
<#macro DetailEditArea paramArea>
<#if isValid(paramArea)>
<#list paramArea["fa:FormObject"] as formObject><@FormRow formObject=formObject/></#list>
</#if>
</#macro>

<#macro FormRow formObject>
<#local formAtom = formObject>
<#local type=formAtom.*[0]?node_name?lower_case>
<#if type !="hidden">
<tr>
	<th width="100" nowrap>${formObject.@label}</th>
	<td><@FormAtom formAtom=formObject/></td>
</tr>
</#if>
</#macro>

<#macro DetailHiddenArea hiddenArea>
<#if isValid(hiddenArea)>
<#list hiddenArea["fa:FormObject"] as formObject><@FormHidden formObject=formObject/></#list>
</#if>
</#macro>

<#macro FormHidden formObject>
<#local formAtom = formObject>
<#local type=formAtom.*[0]?node_name?lower_case>
<#if type="hidden">
<#local atom = formAtom.*[0]>
<#if (isValid(atom["fa:IsPK"])&& "Y" == atom["fa:IsPK"])>
<input type="hidden" id="${atom["fa:Id"]}" name="${atom["fa:Name"]}" value="<%=pageBean.inputValue4DetailOrUpdate("${atom["fa:Name"]}","")%>" />
<#else>
	<@FormAtom formAtom=formObject/>
</#if>
</#if>
</#macro>


<#macro ParameterArea paramArea>
<#if isValid(paramArea)>
<#local formObjects = paramArea["fa:FormObject"]>
<#assign isAllHidden=true />
<#list formObjects as formObject>&nbsp;<@FormObject formObject=formObject/>
<#if formObject.*[0]?node_name?lower_case != "hidden">
<#assign isAllHidden=false />
</#if>
</#list>
<#if (formObjects?size>0)>
<#if (isAllHidden)>
&nbsp;<input type="button" name="button" id="button" value="刷新" class="formbutton" onclick="doQuery()" />
<#else>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</#if>
</#if>
</#if>
</#macro>

<#macro FormObject formObject><#if (isValid(formObject.@label) && formObject.*[0]?node_name?lower_case != "hidden")>${formObject.@label}</#if><@QueryFormAtom formAtom=formObject/></#macro>

<#macro QueryFormAtom formAtom>
<#local type=formAtom.*[0]?node_name?lower_case>
<#local atom = formAtom.*[0]>
<#if type="checkbox">&nbsp;<%=pageBean.selectCheckBox("${atom["fa:Name"]}")%>
<#elseif type="select"><select id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" class="select" onchange="doQuery()"><%=pageBean.selectValue("${atom["fa:Name"]}")%></select>
<#elseif type="radio">&nbsp;<%=pageBean.selectRadio("${atom["fa:Name"]}")%>
<#elseif type="file"><input id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" type="file" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" size="${atom.getSize()}" maxlength="${atom.getMaxLength()}" class="file" />
<#elseif type="hidden"><input type="hidden" id="${atom["fa:Id"]}" name="${atom["fa:Name"]}" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" />
<#elseif type="textarea"><textarea id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("${atom["fa:Name"]}")%></textarea>
<#elseif type="text">
<@InputText formAtom=formAtom/>
<#elseif type="content">
<#if formAtom.@dataType=="date">
&nbsp;<#if formAtom.@format?length==16><%=pageBean.inputTime("${atom["fa:Name"]}")%><#else><%=pageBean.inputDate("${atom["fa:Name"]}")%></#if>
<#else>
&nbsp;<%=pageBean.inputValue("${atom["fa:Name"]}")%>
</#if>
</#if>
</#macro>

<#macro FormAtom formAtom>
<#local type=formAtom.*[0]?node_name?lower_case>
<#local atom = formAtom.*[0]>
<#if type="checkbox">&nbsp;<%=pageBean.selectCheckBox("${atom["fa:Name"]}")%>
<#elseif type="select"><select id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" class="select"><%=pageBean.selectValue("${atom["fa:Name"]}")%></select>
<#elseif type="radio">&nbsp;<%=pageBean.selectRadio("${atom["fa:Name"]}")%>
<#elseif type="popup"><input id="${atom["fa:Id"]}_NAME" name="${atom["fa:Name"]}_NAME" type="text" value="<%=pageBean.inputValue("${atom["fa:Name"]}_NAME")%>" size="${atom["fa:Size"]}" class="text" readonly="readonly" /><input type="hidden" label="${formAtom.@label}" id="${atom["fa:Id"]}" name="${atom["fa:Name"]}" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" /><img id="${getLowerValidName(atom["fa:Name"])}SelectImage" src="images/sta.gif" width="16" height="16" onclick="open${getUpperValidName(atom["fa:Name"])}Box()" />
<#elseif type="resfile"><span style="float:left"><select name="__ResouceList" size="3" multiple="multiple" id="__ResouceList" style="width:300px"></select></span>
<span style="margin-left:2px;padding-left:2px;display: inline-block;">
<div style="margin:1px"><input type="button" name="addResourceBtn" id="addResourceBtn" <%=pageBean.disabled(pageBean.isOnCreateMode())%> value="添加" onclick="openAddResourceRequestBox()" /></div>
<div style="margin:1px"><input type="button" name="delResourceBtn" id="delResourceBtn" <%=pageBean.disabled(pageBean.isOnCreateMode())%> value="删除" onclick="delResourceRequest()" /></div>
</span>
<#elseif type="file"><input id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" type="file" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" size="${atom.getSize()}" maxlength="${atom.getMaxLength()}" class="file" />
<#elseif type="hidden"><input type="hidden" id="${atom["fa:Id"]}" name="${atom["fa:Name"]}" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" />
<#elseif type="textarea"><textarea id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("${atom["fa:Name"]}")%></textarea>
<#elseif type="text">
<@InputText formAtom=formAtom/>
<#elseif type="content">
<#if formAtom.@dataType=="date">
&nbsp;<#if formAtom.@format?length==16><%=pageBean.inputTime("${atom["fa:Name"]}")%><#else><%=pageBean.inputDate("${atom["fa:Name"]}")%></#if>
<#else>
&nbsp;<%=pageBean.inputValue("${atom["fa:Name"]}")%>
</#if>
</#if>
</#macro>

<#macro PoupBoxScript paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#local type=atom?node_name?lower_case>
<#if (type=="popup")>
var ${getLowerValidName(atom["fa:Name"])}Box;
function open${getUpperValidName(atom["fa:Name"])}Box(){
	var handlerId = "${atom["fa:ValueProvider"]}"; 
	if (!${getLowerValidName(atom["fa:Name"])}Box){
		${getLowerValidName(atom["fa:Name"])}Box = new PopupBox('${getLowerValidName(atom["fa:Name"])}Box','请选择${formObject.@label}',{size:'normal',width:'300',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=${atom["fa:Name"]}&targetName=${atom["fa:Name"]}_NAME';
	${getLowerValidName(atom["fa:Name"])}Box.sendRequest(url);
}
</#if>
</#list>
</#macro>

<#macro PoupBoxJavaCode paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#local type=atom?node_name?lower_case>
<#if (type=="popup")>
String ${getLowerValidName(atom["fa:Name"])} = this.getAttributeValue("${atom["fa:Name"]}");
if (!StringUtil.isNullOrEmpty(${getLowerValidName(atom["fa:Name"])})){
	StringBuffer nameBuffer = new StringBuffer();
	//proccess nameBuffer
	setAttribute("${atom["fa:Name"]}_NAME",nameBuffer.toString());
}
</#if>
</#list>
</#macro>

<#macro InputText formAtom>
<#local atom = formAtom.*[0]>
<#if (formAtom.@format="yyyy-MM-dd" || formAtom.@format="yyyy-MM-dd HH:mm")>
<input id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" type="text" value="<#if formAtom.@format?length==16><%=pageBean.inputTime("${atom["fa:Name"]}")%><#else><%=pageBean.inputDate("${atom["fa:Name"]}")%></#if>" size="${atom["fa:Size"]}" class="text" /><img id="${atom["fa:Id"]}Picker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
<#else>
<input id="${atom["fa:Id"]}" label="${formAtom.@label}" name="${atom["fa:Name"]}" type="text" value="<%=pageBean.inputValue("${atom["fa:Name"]}")%>" size="${atom["fa:Size"]}" class="text" />
</#if>
</#macro>

<#macro RowEditInputText formAtom>
<#local atom = formAtom.*[0]>
<#if (formAtom.@format="yyyy-MM-dd" || formAtom.@format="yyyy-MM-dd HH:mm")>
<input id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" type="text" value="<#if formAtom.@format?length==16><%=pageBean.inputTime(i,"${atom["fa:Name"]}")%><#else><%=pageBean.inputDate(i,"${atom["fa:Name"]}")%></#if>" size="${atom["fa:Size"]}" class="text" /><img id="${atom["fa:Id"]}Picker_<%=i%>" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
<#else>
<input id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" type="text" value="<%=pageBean.inputValue(i,"${atom["fa:Name"]}")%>" size="${atom["fa:Size"]}" class="text" />
</#if>
</#macro>

<#macro Validation paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#if (formObject.@format="yyyy-MM-dd" || formObject.@format="yyyy-MM-dd HH:mm")>
<#if formObject.@format?length=10>
initCalendar('${atom["fa:Id"]}','%Y-%m-%d','${atom["fa:Id"]}Picker');
<#else>initCalendar('${atom["fa:Id"]}','%Y-%m-%d %H:%M','${atom["fa:Id"]}Picker');
</#if>
</#if>
</#list>
<#assign dateCheckCount = 0>
<#assign lengCheckCount = 0>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#list formObject["fa:Validation"] as validation>
<#local type=atom?node_name?lower_case>
<#if (validation.@type=="must_not_empty" && type!="hidden")>requiredValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_be_date">datetimeValidators[${dateCheckCount}].set("${formObject.@format}").add("${atom["fa:Id"]}");<#assign dateCheckCount = dateCheckCount + 1>
<#elseif validation.@type=="must_be_int">intValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_be_num">numValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_be_mail">emailValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_only_character">charValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_be_char8number">charNumValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_not_chinese">notChineseValidator.add("${atom["fa:Id"]}");
<#elseif validation.@type=="must_be_char8number8underline">charNumUnderlineValidator.add("${atom["fa:Id"]}");
<#elseif (validation.@type=="length_limit" && type!="hidden")>lengthValidators[${lengCheckCount}].set(${validation.@param0}).add("${atom["fa:Id"]}");<#assign lengCheckCount = lengCheckCount + 1>
</#if>
</#list>
</#list>
</#macro>


<#function existDateField paramArea>
	<#list paramArea["fa:FormObject"] as formObject>
	<#local atom = formObject.*[0]>
	<#if formObject.@dataType="date">
	<#return true>
	</#if>
	</#list>
	<#return false>
</#function>

<#macro InitCalendar paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#if (formObject.@format="yyyy-MM-dd" || formObject.@format="yyyy-MM-dd HH:mm")>
<#if formObject.@format?length=10>
initCalendar('${atom["fa:Id"]}','%Y-%m-%d','${atom["fa:Id"]}Picker');
<#else>initCalendar('${atom["fa:Id"]}','%Y-%m-%d %H:%M','${atom["fa:Id"]}Picker');
</#if>
</#if>
</#list>
</#macro>

<#macro RowInitCalendar paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#if (formObject.@format="yyyy-MM-dd" || formObject.@format="yyyy-MM-dd HH:mm")>
<#if formObject.@format?length=10>
initCalendar('${atom["fa:Id"]}_'+i,'%Y-%m-%d','${atom["fa:Id"]}Picker_'+i);
<#else>initCalendar('${atom["fa:Id"]}_'+i,'%Y-%m-%d %H:%M','${atom["fa:Id"]}Picker_'+i);
</#if>
</#if>
</#list>
</#macro>

<#macro SimpleValidation paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#list formObject["fa:Validation"] as validation>
<#local type=atom?node_name?lower_case>
<#if (validation.@type=="must_not_empty" && type!="hidden")>
if (validation.checkNull($('#${atom["fa:Id"]}').val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"不能为空!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_date">
if (!validation.checkDateTime($('#${atom["fa:Id"]}').val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"日期/时间格式不正确!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_int">
if (!validation.isPositiveInt($('#${atom["fa:Id"]}').val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"必须为整数!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_num">
if (!validation.isData($('#${atom["fa:Id"]}').val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"必须为数字!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_mail">
if (!validation.checkEmail($('#${atom["fa:Id"]}').val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"邮件格式不正确!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif (validation.@type=="length_limit" && type!="hidden")>
if ($('#${atom["fa:Id"]}').val().length > ${validation.@param0}){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"长度不能大于"+${validation.@param0}+"!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_only_character">
if (!validation.isCharacter($("#${atom["fa:Id"]}").val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"只能为字符!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_char8number">
if (!validation.isDigitOrChar($("#${atom["fa:Id"]}").val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"只能包含字符或者数字!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_not_chinese">
if (validation.containChinese($("#${atom["fa:Id"]}").val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"不能包含中文字符!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
<#elseif validation.@type=="must_be_char8number8underline">
if (!validation.checkChar8Num8Underline($("#${atom["fa:Id"]}").val())){
	writeErrorMsg($("#${atom["fa:Id"]}").attr("label")+"只能包含字符、数字或者下划线!");
	selectOrFocus('${atom["fa:Id"]}');
	return false;
}
</#if>
</#list>
</#list>
</#macro>

<#macro RowSimpleValidation paramArea>
<#list paramArea["fa:FormObject"] as formObject>
<#local atom = formObject.*[0]>
<#list formObject["fa:Validation"] as validation>
<#local type=atom?node_name?lower_case>
<#if (validation.@type=="must_not_empty" && type!="hidden")>
if (validation.checkNull($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"不能为空!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_date">
if (!validation.checkDateTime($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"日期/时间格式不正确!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_int">
if (!validation.isPositiveInt($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"必须为整数!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_num">
if (!validation.isData($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"必须为数字!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_mail">
if (!validation.checkEmail($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"邮件格式不正确!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif (validation.@type=="length_limit" && type!="hidden")>
if ($("#${atom["fa:Id"]}"+"_"+i).val().length > ${validation.@param0}){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"长度不能大于"+${validation.@param0}+"!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_only_character">
if (!validation.isCharacter($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"只能为字符!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_char8number">
if (!validation.isDigitOrChar($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"只能包含字符或者数字!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_not_chinese">
if (validation.containChinese($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"不能包含中文字符!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
<#elseif validation.@type=="must_be_char8number8underline">
if (!validation.checkChar8Num8Underline($("#${atom["fa:Id"]}"+"_"+i).val())){
	writeErrorMsg($("#${atom["fa:Id"]}"+"_"+i).attr("label")+"只能包含字符、数字或者下划线!");
	selectOrFocus('${atom["fa:Id"]}'+'_'+i);
	return false;
}
</#if>
</#list>
</#list>
</#macro>


<#macro Column column>
	<ec:column width="${column.@width}" property="${column.@property}" title="${column.@title}" <#if (isValidValue(column.@cell) && column.@cell?string="date")>cell="${column.@cell}"</#if> <#if isValidValue(column.@format)>format="${column.@format}"</#if> <#if isValidValue(column.@mappingItem)>mappingItem="${column.@property}"</#if>/>
</#macro>


<#macro RowEditHiddenArea hiddenArea>
<#if isValid(hiddenArea)>
<#list hiddenArea["fa:FormObject"] as formObject><@RowEditFormHidden formObject=formObject/></#list>
</#if>
</#macro>

<#macro RowEditFormHidden formObject>
<#local formAtom = formObject>
<#local type=formAtom.*[0]?node_name?lower_case>
<#if type="hidden">
<@RowEditFormAtom formAtom=formObject/>
</#if>
</#macro>

<#macro RowEditFormAtom formAtom>
<#local type=formAtom.*[0]?node_name?lower_case>
<#local atom = formAtom.*[0]>
<#if type="checkbox">&nbsp;<%=pageBean.selectCheckBox("${atom["fa:Name"]}")%>
<#elseif type="select"><select id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" class="select"><%=pageBean.selectValue(i,"${atom["fa:Name"]}","${atom["fa:Name"]}Select")%></select>
<#elseif type="radio">&nbsp;<%=pageBean.selectRadio("${atom["fa:Name"]}")%>
<#elseif type="file"><input id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" type="file" value="<%=pageBean.inputValue(i,"${atom["fa:Name"]}")%>" size="${atom.getSize()}" maxlength="${atom.getMaxLength()}" class="file" />
<#elseif type="hidden"><input type="hidden" id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" value="<%=pageBean.inputValue(i,"${atom["fa:Name"]}")%>" />
<#elseif type="textarea"><textarea id="${atom["fa:Id"]}_<%=i%>" label="${formAtom.@label}" name="${atom["fa:Name"]}_<%=i%>" cols="40" rows="3" class="textarea"><%=pageBean.inputValue(i,"${atom["fa:Name"]}")%></textarea>
<#elseif type="text">
<@RowEditInputText formAtom=formAtom/>
<#elseif type="content">
<#if formAtom.@dataType=="date">
&nbsp;<#if formAtom.@format?length==16><%=pageBean.inputTime("${atom["fa:Name"]}")%><#else><%=pageBean.inputDate("${atom["fa:Name"]}")%></#if>
<#else>
&nbsp;<%=pageBean.inputValue("${atom["fa:Name"]}")%>
</#if>
</#if>
</#macro>
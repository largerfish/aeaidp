<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/pickfill",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro PickFillFuncModel>
<#local listTableArea = .node.ListView.ListTableArea>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@listTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function selectRequest(idValue,nameValue){
	parent.ele(ele('targetId').value).value= idValue;
	parent.ele(ele('targetName').value).value= nameValue;
	parent.PopupBox.closeCurrent();
}
function setSelectTempValue(idValue,nameValue){
	ele('targetIdValue').value = idValue;
	ele('targetNameValue').value = nameValue;
}
function doSelectRequest(){
	if (!isValid(ele('targetIdValue').value)){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	<#if (Util.isTrue(.node.BaseInfo.@isMultiSelect))>	
	/*
	var ids = "";
	$("input:[name='The CheckBox Name'][checked]").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	alert(ids);
	*/ 
	</#if>
	selectRequest(ele('targetIdValue').value,ele('targetNameValue').value);
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doSelectRequest()"><input value="&nbsp;" type="button" class="saveImgBtn" title="选择" />选择</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>        
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr><td>
<@Form.ParameterArea paramArea=.node.ListView.ParameterArea></@Form.ParameterArea>
</td></tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" <#if Util.isTrue(listTableArea.@exportCsv)>csvFileName="${.node.BaseInfo.@listTitle}.csv"</#if>
retrieveRowsCallback="process" <#if Util.isTrue(listTableArea.@exportXls)>xlsFileName="${.node.BaseInfo.@listTitle}.xls"</#if>
useAjax="true" <#if Util.isTrue(listTableArea.@sortAble)>sortable="true"</#if>
doPreload="false" toolbarContent="<#if Util.isTrue(listTableArea.@pagination)>navigation|pagejump |pagesize </#if>|export|extend|status"
width="100%" <#if Util.isTrue(listTableArea.@pagination)>rowsDisplayed="10"</#if>
listWidth="100%" 
height="auto" 
>
<#local row =listTableArea["fa:Row"][0]>
<ec:row styleClass="odd" ondblclick="clearSelection();selectRequest('$<#escape x as x?html>{row.<#noescape>${.node.BaseInfo.@codeField}</#noescape>}</#escape>','$<#escape x as x?html>{row.<#noescape>${.node.BaseInfo.@nameField}</#noescape>}</#escape>')" onclick="setSelectTempValue('$<#escape x as x?html>{row.<#noescape>${.node.BaseInfo.@codeField}</#noescape>}</#escape>','$<#escape x as x?html>{row.<#noescape>${.node.BaseInfo.@nameField}</#noescape>}</#escape>')">
	<ec:column width="20" style="text-align:center" property="_0" title="序号" value="${r"${GLOBALROWCOUNT}"}" />
	<#if (Util.isTrue(.node.BaseInfo.@isMultiSelect))>
	<ec:column width="25" style="text-align:center" property="${.node.BaseInfo.@codeField}" cell="checkbox" headerCell="checkbox" />
	</#if>
<#list row["fa:Column"] as columnVar>
<@Form.Column column=columnVar></@Form.Column>
</#list>
</ec:row>
</ec:table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="targetId" id="targetId" value="<%=pageBean.inputValue("targetId")%>" />
<input type="hidden" name="targetName" id="targetName" value="<%=pageBean.inputValue("targetName")%>" />
<input type="hidden" name="targetIdValue" id="targetIdValue" value="" />
<input type="hidden" name="targetNameValue" id="targetNameValue" value="" />
<script language="JavaScript">
<@Form.Validation paramArea=.node.ListView.ParameterArea></@Form.Validation>
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>

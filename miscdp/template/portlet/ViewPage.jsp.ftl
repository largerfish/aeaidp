<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/portlet",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#visit doc>
<#macro PortletFuncModel>
<#local baseInfo = .node.BaseInfo>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<#if (baseInfo.@ajaxLoadData="true")>	
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
%>
</#if>
<div>${r"${someProperty}"}</div>
can you see me ?
<#if (baseInfo.@ajaxLoadData="true")>	
<script type="text/javascript">
$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${r"${getAjaxDataURL}"}',{dataType:'text',onComplete:function(responseText){
			alert(responseText);
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
});
</script> 
</#if>
</#macro>
<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/standard",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro StandardFuncModel>
<#local listTableArea = .node.ListView.ListTableArea>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>${.node.BaseInfo.@listTitle}</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<#if (Util.isValid(.node.ListView.ParameterArea) && Util.isValid(.node.ListView.ParameterArea["fa:FormObject"]))>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
<@Form.ParameterArea paramArea=.node.ListView.ParameterArea></@Form.ParameterArea>
</td></tr></table>
</div>
</#if>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" <#if Util.isTrue(listTableArea.@exportCsv)>csvFileName="${.node.BaseInfo.@listTitle}.csv"</#if>
retrieveRowsCallback="process" <#if Util.isTrue(listTableArea.@exportXls)>xlsFileName="${.node.BaseInfo.@listTitle}.xls"</#if>
useAjax="true" <#if Util.isTrue(listTableArea.@sortAble)>sortable="true"</#if>
doPreload="false" toolbarContent="<#if Util.isTrue(listTableArea.@pagination)>navigation|pagejump |pagesize </#if>|export|extend|status"
width="100%" <#if Util.isTrue(listTableArea.@pagination)>rowsDisplayed="15"</#if>
listWidth="100%" 
height="390px"
>
<#local row =listTableArea["fa:Row"][0]>
<ec:row styleClass="odd" ondblclick="clearSelection();doRequest('viewDetail')" oncontextmenu="selectRow(this,${Util.parseRsIdField(row.@rsIdColumn)});refreshConextmenu()" onclick="selectRow(this,${Util.parseRsIdField(row.@rsIdColumn)})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${r"${GLOBALROWCOUNT}"}" />
<#list row["fa:Column"] as columnVar>
<@Form.Column column=columnVar></@Form.Column>
</#list>
</ec:row>
</ec:table>
<#local rsIds = row.@rsIdColumn?split(",")>
<#list rsIds as rsId>
<input type="hidden" name="${rsId}" id="${rsId}" value="" />
</#list>  
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag(${Util.parseRsIdTag(row.@rsIdColumn)});
var ectableMenu = new EctableMenu('contextMenu','ec_table');
<@Form.Validation paramArea=.node.ListView.ParameterArea></@Form.Validation>
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>

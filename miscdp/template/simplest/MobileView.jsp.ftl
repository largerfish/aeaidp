<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/simplest",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro SimplestFuncModel>
<#local baseInfo = .node.BaseInfo>
<!doctype html>
<%@ page language="java" pageEncoding="UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<meta charset="utf-8">
<title>${.node.BaseInfo.@defaultJspTitle}</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%@include file="/jsp/inc/resource4jqm.inc.jsp"%>
<script type="text/javascript">
function doRedirct(url)
{
	window.location.href = url;
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post" style="padding:5px">
<div id="status" class="status"></div>
<div>
	<ol data-role="listview">
		<%for (int i=0;i < pageBean.getRsList().size();i++){%>
		<li><a href="javascript:doRedirct('SomeURL')" data-ajax="false">测试链接_<%=i%>_<%=pageBean.inputValue(i, "SomeField")%></a></li>
		<%}%>
	</ol>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
</#macro>
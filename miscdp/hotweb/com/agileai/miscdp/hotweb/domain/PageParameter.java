﻿package com.agileai.miscdp.hotweb.domain;

/**
 * 页面参数
 */
public class PageParameter extends PageFormField{
	public static final String[] tagTypes = new String[] {"text","select","radio","hidden"};
	public PageParameter(){
		this.size = 24;
	}
}

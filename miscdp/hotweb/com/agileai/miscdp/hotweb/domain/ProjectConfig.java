﻿package com.agileai.miscdp.hotweb.domain;

import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.ui.wizards.HotwebProjectWizard;
import com.agileai.util.CryptionUtil;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;
/**
 * 工程配置
 */
public class ProjectConfig {
	public static class ProjectType{
		public static final String JavaWebProject = "JavaWebProject";
		public static final String IntegrateWebProject = "IntegrateWebProject";
	}
	
	private String configFile = DeveloperConst.PROJECT_CFG_NAME;
	private String encoding = "utf-8";
	
	private String projectType = ProjectType.JavaWebProject;
	private String projectName = null;
	private String mainPkg = null;
	private String driverUrl = null;
	private String driverClass = null;
	private String userId = null;
	private String userPwd = null;
	private String serverAddress = "";
	private String serverPort = "";
	private String serverUserId = "";
	private String serverUserPwd = "";
	
	private Document document = null;

	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getDriverUrl() {
		return driverUrl;
	}

	public void setDriverUrl(String driverUrl) {
		this.driverUrl = driverUrl;
	}
	public String getDriverClass() {
		return driverClass;
	}
	public void setDriverClass(String driverClass) {
		this.driverClass = driverClass;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
	public String getConfigFile() {
		return configFile;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	public String getServerPort() {
		return serverPort;
	}
	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}
	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getMainPkg() {
		return mainPkg;
	}
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	public String getServerUserId() {
		return serverUserId;
	}
	public void setServerUserId(String serverUserId) {
		this.serverUserId = serverUserId;
	}
	public String getServerUserPwd() {
		return serverUserPwd;
	}
	public void setServerUserPwd(String serverUserPwd) {
		this.serverUserPwd = serverUserPwd;
	}
	
	public void initConfig(){
		String userPwdNodePath = "//HotwebModule/DBConfig/UserPwd";
		Node pwdNode = this.getDocument().selectSingleNode(userPwdNodePath);
		String encrptPassword = pwdNode.getText(); 
		String secretKey = HotwebProjectWizard.getSecurityKey();
		this.userPwd = CryptionUtil.decryption(encrptPassword, secretKey);
		
		String userNodePath = "//HotwebModule/DBConfig/UserId";
		Node userNode = this.getDocument().selectSingleNode(userNodePath);
		this.userId = userNode.getText();
		
		String driverNodePath = "//HotwebModule/DBConfig/DriverClass";
		Node driverNode = this.getDocument().selectSingleNode(driverNodePath);
		this.driverClass = driverNode.getText();
		
		String urlNodePath = "//HotwebModule/DBConfig/DriverUrl";
		Node urlNode = this.getDocument().selectSingleNode(urlNodePath);
		this.driverUrl = urlNode.getText();
		
//		String drvierPathNodePath = "//HotwebModule/DBConfig/ClassPathEntry";
//		Node drvierPathNode = this.getDocument().selectSingleNode(drvierPathNodePath);
//		if (drvierPathNode != null){
//			this.classPathEntry = drvierPathNode.getText().split(";");	
//		}
		
		String serverAdressNodePath = "//HotwebModule/ServerConfig/ServerAddress";
		Node serverAdressNode = this.getDocument().selectSingleNode(serverAdressNodePath);
		if (serverAdressNode != null){
			this.serverAddress = serverAdressNode.getText();			
		}
		String serverPortNodePath = "//HotwebModule/ServerConfig/ServerPort";
		Node serverPortNode = this.getDocument().selectSingleNode(serverPortNodePath);
		if (serverPortNode != null){
			this.serverPort = serverPortNode.getText();			
		}
		
		String serverUserIdNodePath = "//HotwebModule/ServerConfig/ServerUserId";
		Node serverUserIdNode = this.getDocument().selectSingleNode(serverUserIdNodePath);
		if (serverUserIdNode != null){
			this.serverUserId = serverUserIdNode.getText();			
		}	
		String serverUserPwdNodePath = "//HotwebModule/ServerConfig/ServerUserPwd";
		Node serverUserPwdNode = this.getDocument().selectSingleNode(serverUserPwdNodePath);
		if (serverUserPwdNode != null){
			String encrptServerUserPwd = serverUserPwdNode.getText();	
			this.serverUserPwd = CryptionUtil.decryption(encrptServerUserPwd, secretKey);
		}	
		
		String projectTypeNodePath = "//HotwebModule/ProjectType";
		Node projectTypeNode = this.getDocument().selectSingleNode(projectTypeNodePath);
		if (projectTypeNode != null){
			this.projectType = projectTypeNode.getText();			
		}
		
		String mainPkgNodePath = "//HotwebModule/MainPkg";
		Node mainPkgNode = this.getDocument().selectSingleNode(mainPkgNodePath);
		this.mainPkg = mainPkgNode.getText();
	}
	
	public boolean isIntegrateWebProject(){
		return ProjectType.IntegrateWebProject.equals(this.projectType);
	}
	
    public Document getDocument(){
        Document result = null;
        try {
            if (this.document == null){
            	this.document = XmlUtil.readDocument(this.configFile);
            }
            result = document;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
    
    public void saveHotwebConfig() throws IOException{
        Document document = XmlUtil.createDocument();
        Element rootElement = document.addElement("HotwebModule");
        Element projectType = rootElement.addElement("ProjectType");
        projectType.setText(this.projectType);
        
        Element mainPkg = rootElement.addElement("MainPkg");
        mainPkg.setText(this.mainPkg);
        
        Element dbConfig = rootElement.addElement("DBConfig");
        dbConfig.addElement("DriverUrl").setText(this.driverUrl);
        dbConfig.addElement("DriverClass").setText(this.driverClass);
        dbConfig.addElement("UserId").setText(this.userId);
        String secretKey = HotwebProjectWizard.getSecurityKey();
        String crytPassword = CryptionUtil.encryption(this.userPwd, secretKey);
        dbConfig.addElement("UserPwd").setText(crytPassword);
//        dbConfig.addElement("ClassPathEntry").setText(MiscdpUtil.toString(this.classPathEntry,";"));
        
        Element serverConfig = rootElement.addElement("ServerConfig");
        serverConfig.addElement("ServerAddress").setText(this.serverAddress);
        serverConfig.addElement("ServerPort").setText(this.serverPort);
        if (StringUtil.isNotNullNotEmpty(this.serverUserId)){
        	serverConfig.addElement("ServerUserId").setText(this.serverUserId);
        }
        if (StringUtil.isNotNullNotEmpty(this.serverUserPwd)){
        	 String crytServerPassword = CryptionUtil.encryption(this.serverUserPwd, secretKey);
        	serverConfig.addElement("ServerUserPwd").setText(crytServerPassword);
        }
        XmlUtil.writeDocument(document, encoding, configFile);
        this.document = document;
    }
    
    public void addInitFuncTree() throws IOException{
    	Element element = this.document.getRootElement().addElement("FuncTree");
    	String nextId = String.valueOf(System.currentTimeMillis());
    	element.addElement("FuncList").addAttribute("id",nextId).addAttribute("name","样例模块").addAttribute("code", "sample");
    	XmlUtil.writeDocument(document, encoding, configFile);
    }
}
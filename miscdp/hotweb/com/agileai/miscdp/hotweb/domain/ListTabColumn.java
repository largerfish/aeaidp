﻿package com.agileai.miscdp.hotweb.domain;
/**
 * 页面列表中的列
 */
public class ListTabColumn {
	public static final String[] columnProperties = new String[] {"Title", "Property","Width",
		"Cell","Format","MappingItem"};
	public static final String[] dataTypes = new String[] {"string","date","numeric","other"};
	public static final String[] formats = new String[] {"","yyyy-MM-dd","yyyy-MM-dd HH:mm", "0.0","0.00","0.000"};
	public static final String[] widths = new String[] {"","50","80","100","150","200"};
	
	public static final String TITLE= "Title";
	public static final String PROPERTY = "Property";
	public static final String WIDTH = "Width";
	public static final String CELL = "Cell";
	public static final String FORMAT = "Format";
	public static final String PROVIDER = "Provider";
	public static final String MAPPINGITEM = "MappingItem";
	
	private String title= null;
	private String property = null;
	private String width = null;
	private String cell = null;
	private String format = null;
	private String mappingItem = null;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String code) {
		this.property = code;
	}
	public String getCell() {
		return cell;
	}
	public void setCell(String dataType) {
		this.cell = dataType;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String dataFormat) {
		this.format = dataFormat;
	}
	public String getMappingItem() {
		return mappingItem;
	}
	public void setMappingItem(String valueProvider) {
		this.mappingItem = valueProvider;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
}

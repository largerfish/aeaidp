package com.agileai.miscdp.hotweb.domain;


public class DeployResource {
	public static final class DeployTypes{
		public static final String Application = "Application";
		public static final String Modules = "Modules";
		public static final String AppFiles = "AppFiles";
	}
	public static final class FileTypes{
		public static final String Applicaiton = "applicaiton";
		public static final String Module = "module";
		public static final String Folder = "folder";
		public static final String Package = "package";
		public static final String Java = "java";
		public static final String Files = "files";
	}
	
	public static String[] columnProperties = new String[]{"name","type"};
	
	private String fileName = null;
	private String fileType = null;
	private String filePath = null;
	private String compilePath = null;
	private boolean isSource = false;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getCompilePath() {
		return compilePath;
	}
	public void setCompilePath(String compilePath) {
		this.compilePath = compilePath;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public boolean isSource() {
		return isSource;
	}
	public void setSource(boolean isSource) {
		this.isSource = isSource;
	}
}
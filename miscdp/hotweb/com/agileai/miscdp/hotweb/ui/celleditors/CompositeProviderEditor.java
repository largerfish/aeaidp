package com.agileai.miscdp.hotweb.ui.celleditors;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.CompositeProviderDialog;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.PopupProviderDialog;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.ResourceFileProviderDialog;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;

public class CompositeProviderEditor extends DialogCellEditor{
	Table table = null;
	BaseModelEditor baseModelEditor;
	TableViewer tableViewer;
	
    public CompositeProviderEditor(BaseModelEditor baseModelEditor,TableViewer tableViewer,Composite parent) {
        this(parent, SWT.NONE);
        this.table = (Table)parent;
        this.baseModelEditor = baseModelEditor;
        this.tableViewer = tableViewer;
    }
    public CompositeProviderEditor(Composite parent, int style) {
        super(parent, style);
    }
	protected Object openDialogBox(Control cellEditorWindow) {
		String result = null;
		
		String projectName = baseModelEditor.getFuncModel().getProjectName();
		TableItem[] tableItems = table.getSelection();
		TableItem item = (TableItem)tableItems[0];
		PageFormField pageParameter = (PageFormField) item.getData(); 
		String tagType = (String)pageParameter.getTagType();
		
		if (!PageFormField.RES_FILE_TAG.equals(tagType)){
			if ("popup".equals(tagType)){
				PopupProviderDialog dialog = new PopupProviderDialog(baseModelEditor,this.getControl().getShell(),projectName);
				dialog.setPageParameter(pageParameter);
				dialog.open();
				if (dialog.getReturnCode() == Dialog.OK){
					result = dialog.getCode();
					pageParameter.setCode(result);
					baseModelEditor.setModified(true);
					baseModelEditor.fireDirty();
					tableViewer.refresh();
				}
			}else{
				CompositeProviderDialog dialog = new CompositeProviderDialog(this.getControl().getShell(),projectName);
				if ("select".equals(tagType) || "radio".equals(tagType) || "checkbox".equals(tagType)){
					dialog.setCanInitInput(true);
				}
				dialog.setPageParameter(pageParameter);
				dialog.open();
				result = dialog.getCode();
				if (!dialog.isCancelEdit()){
					pageParameter.setCode(result);
					baseModelEditor.setModified(true);
					baseModelEditor.fireDirty();
					tableViewer.refresh();
				}				
			}
		} 
		else{
			ResourceFileProviderDialog dialog = new ResourceFileProviderDialog(baseModelEditor,this.getControl().getShell(),projectName);
			dialog.setPageParameter(pageParameter);
			dialog.open();
			if (dialog.getReturnCode() == Dialog.OK){
				result = dialog.getCode();
				pageParameter.setCode(result);
				baseModelEditor.setModified(true);
				baseModelEditor.fireDirty();
				tableViewer.refresh();
			}
		}
		return result;
	}
}
package com.agileai.miscdp.hotweb.ui.editors.standard;

import java.util.List;

import net.sf.swtaddons.autocomplete.combo.AutocompleteComboInput;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.standard.StandardFuncModel;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.util.ResourceUtil;
/**
 * 基础配置页面
 */
public class SMBasicConfigPage extends Composite implements Modifier {
	private StandardModelEditor modelEditor = null;
	private Text funcNameText;
	private Text funcTypeText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private SMExtendAttribute sMExtendAttribute;
	private Text listSqlText;
	private Combo tableNameCombo;

	private StandardFuncModel funcModel= null;
	private Text funcSubPkgText;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void setTableComboValues(String schema) {
		String projectName = this.funcModel.getProjectName();
		List tableList = DBManager.getInstance(projectName).getTables(schema);
		String[] tables = (String[]) tableList.toArray(new String[0]);
		this.tableNameCombo.setItems(tables);
	}
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public SMBasicConfigPage(Composite parent,StandardModelEditor modelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();
//		createContent();
	}
	public void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		Label label_2 = new Label(this, SWT.NONE);
		label_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_2.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		funcSubPkgText.setEditable(false);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);

		final Label tablenameLabel = new Label(this, SWT.NONE);
		tablenameLabel.setText("数据表");

		tableNameCombo = new Combo(this, SWT.NONE);
		tableNameCombo.setForeground(ResourceUtil.getColor(255, 255, 255));
		tableNameCombo.setBackground(ResourceUtil.getColor(128, 0, 255));
		tableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		new AutocompleteComboInput(tableNameCombo);
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_tabFolder.heightHint = 254;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("列表SQL");

		listSqlText = new Text(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
		toolkit.adapt(listSqlText, true, true);
		tabItem_1.setControl(listSqlText);

		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		sMExtendAttribute = new SMExtendAttribute(tabFolder,SWT.NONE);
		tabItem.setControl(sMExtendAttribute);
		setTableComboValues(null);
	}
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public Combo getTableNameCombo() {
		return tableNameCombo;
	}

	public StandardFuncModel buildConfig(){
		String tableName = tableNameCombo.getText();
		funcModel.setTableName(tableName);
		funcModel.setFuncName(funcNameText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setListSql(listSqlText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		funcModel.setServiceId(sMExtendAttribute.getServiceIdText().getText());
		funcModel.setImplClassName(sMExtendAttribute.getImplClassNameText().getText());
		funcModel.setInterfaceName(sMExtendAttribute.getInterfaceNameText().getText());
		funcModel.setListJspName(sMExtendAttribute.getListJspNameText().getText());
		funcModel.setDetailJspName(sMExtendAttribute.getDetailJspNameText().getText());

		funcModel.setExportCsv(sMExtendAttribute.getExportCsvBtn().getSelection());
		funcModel.setExportXls(sMExtendAttribute.getExportXslBtn().getSelection());
		funcModel.setPagination(sMExtendAttribute.getPaginationBtn().getSelection());
		funcModel.setSortAble(sMExtendAttribute.getSortListBtn().getSelection());
		funcModel.setTemplate(sMExtendAttribute.getTemplateCombo().getItem(0));
		return this.funcModel;
	}

	public void initValues() {
		int tableCount = this.tableNameCombo.getItemCount();
		if (funcModel.getTableName() != null){
			for (int i=0;i < tableCount;i++){
				String tableName = this.tableNameCombo.getItem(i).toLowerCase();
				if (this.funcModel.getTableName().toLowerCase().equals(tableName)){
					this.tableNameCombo.select(i);
					break;
				}
			}
		}
		funcNameText.setText(this.funcModel.getFuncName());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		listSqlText.setText(this.funcModel.getListSql().trim());
		sMExtendAttribute.getServiceIdText().setText(funcModel.getServiceId());
		sMExtendAttribute.getImplClassNameText().setText(funcModel.getImplClassName());
		sMExtendAttribute.getInterfaceNameText().setText(funcModel.getInterfaceName());
		sMExtendAttribute.getListJspNameText().setText(funcModel.getListJspName());
		sMExtendAttribute.getDetailJspNameText().setText(funcModel.getDetailJspName());
		sMExtendAttribute.getTemplateCombo().select(0);
		
		sMExtendAttribute.getExportCsvBtn().setSelection(funcModel.isExportCsv());
		sMExtendAttribute.getExportXslBtn().setSelection(funcModel.isExportXls());
		sMExtendAttribute.getPaginationBtn().setSelection(funcModel.isPagination());
		sMExtendAttribute.getSortListBtn().setSelection(funcModel.isSortAble());
	}
	public Text getListSqlText() {
		return listSqlText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
//		funcSubPkgText.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent e) {
//				if (!funcSubPkgText.getText().equals(funcModel.getFuncSubPkg())){
//					modelEditor.setModified(true);
//					modelEditor.fireDirty();				
//				}
//			}
//		});
		sMExtendAttribute.getPaginationBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getSortListBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getExportCsvBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getExportXslBtn().addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getDetailJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getListJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		sMExtendAttribute.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		tableNameCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		listSqlText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
}

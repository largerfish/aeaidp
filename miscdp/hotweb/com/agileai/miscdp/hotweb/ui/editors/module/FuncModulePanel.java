package com.agileai.miscdp.hotweb.ui.editors.module;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.ui.TextCheckPolicy;

/**
 * 功能模块编辑器容器
 */
public class FuncModulePanel extends Composite {
	private Text moduleDescText;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text moduleNameText;
	private Text moduleCodeText;
	private FuncModuleEditor moduleEditorPart; 
	private boolean hasChildren = false;

	public FuncModulePanel(Composite parent, int style,FuncModuleEditor editorPart,boolean hasChildren) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.moduleEditorPart = editorPart;
		this.hasChildren = hasChildren; 
		
		this.createContent();
	}

	private void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		setLayout(gridLayout);

		final Label label = new Label(this, SWT.NONE);
		label.setText("模块名称");

		moduleNameText = new Text(this, SWT.BORDER);
		moduleNameText.setText(moduleEditorPart.getFuncModule().getModuleName());
		moduleNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				moduleEditorPart.setModified(true);
				moduleEditorPart.fireDirty();
				moduleEditorPart.getFuncModule().setModuleName(moduleNameText.getText());
			}
		});
		moduleNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(moduleNameText, true, true);

		
		final Label label_0 = new Label(this, SWT.NONE);
		label_0.setText("模块编码");

		moduleCodeText = new Text(this, SWT.BORDER);
		moduleCodeText.setText(moduleEditorPart.getFuncModule().getModuleCode());
		if (!hasChildren){
			moduleCodeText.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					moduleEditorPart.setModified(true);
					moduleEditorPart.fireDirty();
					moduleEditorPart.getFuncModule().setModuleCode(moduleCodeText.getText());
				}
			});
			new TextCheckPolicy(moduleCodeText,TextCheckPolicy.POLICY_PACKAGE_NAME);
		}else{
			moduleCodeText.setEditable(false);
		}
		moduleCodeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		toolkit.adapt(moduleCodeText, true, true);
		
		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("模块描述");

		moduleDescText = new Text(this, SWT.MULTI | SWT.READ_ONLY | SWT.BORDER);
		moduleDescText.setText("这是一个功能模块（功能目录），不能进行编辑修改操作！");
		toolkit.adapt(moduleDescText, true, true);
		final GridData gd_funcTypeText_1 = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_funcTypeText_1.heightHint = 88;
		moduleDescText.setLayoutData(gd_funcTypeText_1);
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

	public Text getModuleNameText() {
		return moduleNameText;
	}
	public Text getModuleCodeText() {
		return moduleCodeText;
	}	
}

﻿package com.agileai.miscdp.hotweb.ui.editors.query;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchActionConstants;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.ui.actions.query.QMGenCodesAction;
import com.agileai.miscdp.hotweb.ui.actions.query.QMInitGridAction;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditorContributor;
import com.agileai.miscdp.util.ImageUtil;
/**
 * 查询操作模型贡献者
 */
public class QueryModelEditorContributor extends BaseModelEditorContributor {
	public QueryModelEditorContributor() {
		super();
	}
	protected void createActions() {
		initGridAction = new QMInitGridAction();
		initGridAction.setText("初始数据");
		initGridAction.setToolTipText("初始化功能模型基本数据");
		ImageDescriptor initGridImageDesc = ImageUtil.getDescriptor("initgrid");
		initGridAction.setImageDescriptor(initGridImageDesc);
		((QMInitGridAction)initGridAction).setContributor(this);
		
		genCodesAction = new QMGenCodesAction();
		genCodesAction.setText("生成代码");
		genCodesAction.setToolTipText("生成代码及配置信息");
		ImageDescriptor genCodesImageDesc = ImageUtil.getDescriptor("gencodes");
		genCodesAction.setImageDescriptor(genCodesImageDesc);
		((QMGenCodesAction)genCodesAction).setContributor(this);
	}
	
	public void contributeToMenu(IMenuManager manager) {
		String path = "MisDevMenu"; 
		IMenuManager menu = manager.findMenuUsingPath(path);
		if (menu != null){
			manager.prependToGroup(IWorkbenchActionConstants.MB_ADDITIONS, menu);
			menu.add(initGridAction);
			initGridAction.setEnabled(true);
			menu.insertBefore(DeveloperConst.SHOW_PERSPECTIVE_ACTION_ID,initGridAction);
			
			menu.add(genCodesAction);
			genCodesAction.setEnabled(true);
			menu.insertBefore(DeveloperConst.SHOW_PERSPECTIVE_ACTION_ID,genCodesAction);
		}
	}
	public void contributeToToolBar(IToolBarManager manager) {
		manager.add(new Separator());
		manager.add(initGridAction);
		manager.add(genCodesAction);
	}
}

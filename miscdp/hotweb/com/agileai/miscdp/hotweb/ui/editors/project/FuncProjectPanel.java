package com.agileai.miscdp.hotweb.ui.editors.project;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.server.HotServerManage;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;

/**
 * 功能工程编辑器容器
 */
public class FuncProjectPanel extends Composite {
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text projectNameText;
	private FuncProjectEditor projectEditorPart; 
	private Label label_1;
	private Text serverAddressText;
	private Label label_2;
	private Text serverPortText;
	private Label projectTypeLabel;
	private Text projectTypeText;
	private Label driverLabel;
	private Text driverClassText;
	private Label jdbcURLLabel;
	private Text driverConnURLText;
	private Label dbUserIdLable;
	private Text dbUserIdText;
	private Label lblNewLabel_3;
	private Text dbUserPwdText;
	private Group serverConfigGroup;
	private Group dbConfigGroup;
	private Label lblNewLabel;
	private Text serverUserIdText;
	private Label lblNewLabel_1;
	private Text serverUserPwdText;
	private Button testServerButton;
	private Button testDatabaseButton;

	public FuncProjectPanel(Composite parent, int style,FuncProjectEditor editorPart) {
		super(parent, style);
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		this.projectEditorPart = editorPart;
		
		this.createContent();
		this.initValues();
		this.registryModifier();
	}
	
	protected void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 7;
		gridLayout.numColumns = 2;
		setLayout(gridLayout);
		
		serverConfigGroup = new Group(this, SWT.NONE);
		serverConfigGroup.setText("服务器连接配置");
		GridLayout gl_serverConfigGroup = new GridLayout(3, false);
		gl_serverConfigGroup.horizontalSpacing = 3;
		serverConfigGroup.setLayout(gl_serverConfigGroup);
		serverConfigGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		toolkit.adapt(serverConfigGroup);
		toolkit.paintBordersFor(serverConfigGroup);
		
		projectTypeLabel = new Label(serverConfigGroup, SWT.NONE);
		projectTypeLabel.setText("应用类型");
		
		projectTypeText = new Text(serverConfigGroup, SWT.BORDER);
		projectTypeText.setEnabled(false);
		projectTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		projectTypeText.setEditable(false);
		toolkit.adapt(projectTypeText, true, true);
		new Label(serverConfigGroup, SWT.NONE);
				
		final Label label = new Label(serverConfigGroup, SWT.NONE);
		label.setText("应用名称");

		projectNameText = new Text(serverConfigGroup, SWT.BORDER);
		projectNameText.setEditable(false);
		projectNameText.setEnabled(false);
		projectNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		projectNameText.setText(projectEditorPart.getFuncProject().getProjectName());
		toolkit.adapt(projectNameText, true, true);
		new Label(serverConfigGroup, SWT.NONE);
		
		label_1 = new Label(serverConfigGroup, SWT.NONE);
		label_1.setText("服务器地址");
		
		serverAddressText = new Text(serverConfigGroup, SWT.BORDER);
		serverAddressText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		serverAddressText.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		toolkit.adapt(serverAddressText, true, true);
		new Label(serverConfigGroup, SWT.NONE);
		
		label_2 = new Label(serverConfigGroup, SWT.NONE);
		label_2.setText("服务器端口");
		
		serverPortText = new Text(serverConfigGroup, SWT.BORDER);
		serverPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(serverPortText, true, true);
		new Label(serverConfigGroup, SWT.NONE);
		
		lblNewLabel = new Label(serverConfigGroup, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("服务器用户");
		
		serverUserIdText = new Text(serverConfigGroup, SWT.BORDER);
		serverUserIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(serverUserIdText, true, true);
		new Label(serverConfigGroup, SWT.NONE);
		
		lblNewLabel_1 = new Label(serverConfigGroup, SWT.NONE);
		lblNewLabel_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel_1.setText("服务器密码");
		
		serverUserPwdText = new Text(serverConfigGroup, SWT.BORDER | SWT.PASSWORD);
		serverUserPwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(serverUserPwdText, true, true);
		
		testServerButton = new Button(serverConfigGroup, SWT.NONE);
		testServerButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					String appName = projectNameText.getText();
					HotServerManage hotServerManage = MiscdpUtil.getHotServerManage(appName);
					hotServerManage.isExistContext(appName);
					DialogUtil.showMessage(getShell(),"OK","连接服务器成功！",DialogUtil.MESSAGE_TYPE.INFO);
				} catch (Exception ex) {
					DialogUtil.showMessage(getShell(),"消息提示","连接服务器失败！",DialogUtil.MESSAGE_TYPE.WARN);
					MiscdpPlugin.getDefault().logError(ex.getLocalizedMessage(), ex);
				}
			}
		});
		toolkit.adapt(testServerButton, true, true);
		testServerButton.setText("测试连接");
		
		dbConfigGroup = new Group(this, SWT.NONE);
		dbConfigGroup.setText("数据库连接配置");
		GridLayout gl_dbConfigGroup = new GridLayout(3, false);
		gl_dbConfigGroup.horizontalSpacing = 3;
		dbConfigGroup.setLayout(gl_dbConfigGroup);
		dbConfigGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 2, 1));
		toolkit.adapt(dbConfigGroup);
		toolkit.paintBordersFor(dbConfigGroup);
		
		driverLabel = new Label(dbConfigGroup, SWT.NONE);
		driverLabel.setText("数据库驱动");
		
		driverClassText = new Text(dbConfigGroup, SWT.BORDER);
		driverClassText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(driverClassText, true, true);
		new Label(dbConfigGroup, SWT.NONE);
		
		jdbcURLLabel = new Label(dbConfigGroup, SWT.NONE);
		jdbcURLLabel.setText("数据库连接");
		
		driverConnURLText = new Text(dbConfigGroup, SWT.BORDER);
		driverConnURLText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(driverConnURLText, true, true);
		new Label(dbConfigGroup, SWT.NONE);
		
		dbUserIdLable = new Label(dbConfigGroup, SWT.NONE);
		dbUserIdLable.setText("数据库用户");
		
		dbUserIdText = new Text(dbConfigGroup, SWT.BORDER);
		dbUserIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(dbUserIdText, true, true);
		new Label(dbConfigGroup, SWT.NONE);
		
		lblNewLabel_3 = new Label(dbConfigGroup, SWT.NONE);
		lblNewLabel_3.setText("数据库密码");
		
		dbUserPwdText = new Text(dbConfigGroup, SWT.BORDER | SWT.PASSWORD);
		dbUserPwdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(dbUserPwdText, true, true);
		
		testDatabaseButton = new Button(dbConfigGroup, SWT.NONE);
		testDatabaseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String driverClass = driverClassText.getText();
				String driverUrl = driverConnURLText.getText();
				String userId = dbUserIdText.getText();
				String userPwd = dbUserPwdText.getText();
				String databaseType = MiscdpUtil.parseDataBaseType(driverUrl);
				String[] driverJars = MiscdpUtil.getDriverJarArray(databaseType);
				if (!MiscdpUtil.isValidConnection(driverClass,driverUrl,userId,userPwd,driverJars)){
					MessageDialog.openInformation(getShell(),"提示信息","不能连接，请确认数据库连接信息填写正确！");
				}else{
					MessageDialog.openInformation(getShell(),"OK","数据连接成功！");	
				}
			}
		});
		toolkit.adapt(testDatabaseButton, true, true);
		testDatabaseButton.setText("测试连接");
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}
	private void initValues(){
		String projectName = projectEditorPart.getFuncProject().getProjectName();
		ProjectConfig projectConfig = MiscdpUtil.getProjectConfig(projectName);
		String serverPort = projectConfig.getServerPort();
		String serverAddress = projectConfig.getServerAddress();
		
		serverPortText.setText(serverPort);
		serverAddressText.setText(serverAddress);
		if (projectConfig.isIntegrateWebProject()){
			projectTypeText.setText("集成Web项目");
		}else{
			projectTypeText.setText("普通Web项目");
		}
		serverUserIdText.setText(projectConfig.getServerUserId());
		serverUserPwdText.setText(projectConfig.getServerUserPwd());
		
		driverClassText.setText(projectConfig.getDriverClass());
		driverConnURLText.setText(projectConfig.getDriverUrl());
		dbUserIdText.setText(projectConfig.getUserId());
		dbUserPwdText.setText(projectConfig.getUserPwd());
	}
	
	private void registryModifier(){
		serverPortText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});		
		serverAddressText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});
		serverUserIdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});
		serverUserPwdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});
		
		
		driverClassText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});	
		driverConnURLText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});	
		dbUserIdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});	
		dbUserPwdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent arg0) {
				projectEditorPart.setModified(true);
				projectEditorPart.fireDirty();
			}
		});		
	}

	@Override
	protected void checkSubclass() {
	}

	public Text getServerPortText(){
		return serverPortText;
	}
	public Text getServerAddressText(){
		return serverAddressText;
	}
	public Text getServerUserIdText(){
		return serverUserIdText;
	}	
	public Text getServerUserPwdText(){
		return serverUserPwdText;
	}	
	
	public Text getDriverClassText(){
		return driverClassText;
	}
	public Text getDriverConnURLText(){
		return driverConnURLText;
	}
	public Text getDbUserIdText(){
		return dbUserIdText;
	}
	public Text getDbUserPwdText(){
		return dbUserPwdText;
	}	
}

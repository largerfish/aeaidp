package com.agileai.miscdp.hotweb.ui.actions;

import org.eclipse.jface.action.Action;

import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditorContributor;
/**
 * 初始化表中功能模型数据
 */
abstract public class BaseInitGridAction extends Action{
	protected BaseModelEditorContributor contributor;
	protected BaseModelEditor modelEditor;
	
	public BaseInitGridAction() {
	}
	public void setContributor(BaseModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	abstract public void run();
}

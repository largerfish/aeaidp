﻿package com.agileai.miscdp.hotweb.ui.views;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.part.ViewPart;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * 功能树视图内容提供器
 */
public class TreeViewContentProvider implements IStructuredContentProvider,
		ITreeContentProvider {
	private LinkedHashMap<String,TreeObject> projectTreeObjectMap = null;
	protected ViewPart viewPart = null;
	
	public TreeViewContentProvider(ViewPart viewPart){
		super();
		this.viewPart = viewPart;
	}
	public Object[] getElements(Object parent) {
		if (projectTreeObjectMap == null){
			initialize();			
		}
		if (parent instanceof TreeObject){
			return getChildren(parent);			
		}else{
			return projectTreeObjectMap.values().toArray();
		}
	}
	
	public TreeObject getProjectTreeObject(String projectName){
		return projectTreeObjectMap.get(projectName);
	}
	
	public Object getParent(Object child) {
		return ((TreeObject) child).getParent();
	}

	public Object[] getChildren(Object parent) {
		return ((TreeObject) parent).getChildren();
	}

	public boolean hasChildren(Object parent) {
		return ((TreeObject) parent).hasChildren();
	}

	public void initialize() {
		try {
			projectTreeObjectMap = new LinkedHashMap<String,TreeObject>();
			IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
			if (projects != null){
				for (int i=0;i < projects.length;i++){
					IProject project = projects[i];
					if (!project.isOpen()){
						continue;
					}
					boolean hasSoakerNature = project.hasNature(DeveloperConst.HOTWEB_NATURE_ID);
					if (!hasSoakerNature){
						continue;
					}
					String projectName = project.getName();
					TreeObject projectNode = new TreeObject(String.valueOf(i),projectName);
					projectNode.setNodeType(TreeObject.NODE_PROJECT);
					projectNode.setProjectName(projectName);
					
					projectTreeObjectMap.put(projectName, projectNode);
					
					String xmlFile = MiscdpUtil.getCfgFileName(projectName,DeveloperConst.PROJECT_CFG_NAME);
					File configFile = new File(xmlFile);
					if (!configFile.exists()){
						continue;
					}
					Document doc = XmlUtil.readDocument(xmlFile);
					Element rootElement = doc.getRootElement();
					String funcTreeNodePath = "//HotwebModule/FuncTree";
					Node funcTreeNode = rootElement.selectSingleNode(funcTreeNodePath);
					Element treeElement = (Element)funcTreeNode;
					
					this.initTree(treeElement, projectNode, project.getName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void initTree(Element parentElement, TreeObject treeNode,String projectName) {
		List elments = parentElement.elements();
		for (int i = 0; i < elments.size(); i++) {
			Element element = (Element)elments.get(i);
			String nodeTypeName = element.getName();
			String nodeId = null;
			String nodeCode = null;
			String nodeName = null;
			
			if (DeveloperConst.FuncTreeXml.FUNC_LIST_NAME.equals(nodeTypeName)){
				nodeId = element.attributeValue(DeveloperConst.FuncTreeXml.ID);
				nodeName = element.attributeValue(DeveloperConst.FuncTreeXml.NAME);
				nodeCode = element.attributeValue(DeveloperConst.FuncTreeXml.CODE);
				TreeObject childNode = new TreeObject(nodeId,nodeName);
				childNode.setProjectName(projectName);
				childNode.setNodeType(TreeObject.NODE_COMPOSITE);
				childNode.setFuncSubPkg(nodeCode);
				treeNode.addChild(childNode);
				this.initTree(element,childNode,projectName);				
			}
			else if (DeveloperConst.FuncTreeXml.FUNC_NODE_NAME.equals(nodeTypeName)){
				nodeId = element.attributeValue(DeveloperConst.FuncTreeXml.ID);
				nodeName = element.attributeValue(DeveloperConst.FuncTreeXml.NAME);
				String funcType = element.attributeValue(DeveloperConst.FuncTreeXml.TYPE);
				String funcTypeName = element.attributeValue(DeveloperConst.FuncTreeXml.TYPE_NAME);
				String funcSubPkg = element.attributeValue(DeveloperConst.FuncTreeXml.SUBPKG);
				
				TreeObject childNode = new TreeObject(nodeId,nodeName);
				childNode.setProjectName(projectName);
				childNode.setFuncType(funcType);
				childNode.setFuncTypeName(funcTypeName);
				childNode.setFuncSubPkg(funcSubPkg);
				childNode.setNodeType(TreeObject.NODE_LEAF);
				treeNode.addChild(childNode);
			}
		}
	}
	
	public void dispose() {
	}
	
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}
}

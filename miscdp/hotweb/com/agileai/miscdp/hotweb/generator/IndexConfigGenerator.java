package com.agileai.miscdp.hotweb.generator;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.util.XmlUtil;

public class IndexConfigGenerator {
	private String basePath = null;
	private String moduleName = null;
	
	public IndexConfigGenerator(String projectName,String moduleName){
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		this.basePath = project.getLocation().toString()+"/WebRoot/WEB-INF/";
		this.moduleName = moduleName;
	}
	
	public void generateHandlerIndexConfig(String handlerId){
		try {
			String configFile = this.basePath + "HandlerIndex.xml";
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        XPath xpath = document.createXPath("//Handlers/bean[@id='"+handlerId+"']");
	        Node beanNode = xpath.selectSingleNode(document);
	        if (beanNode != null){
	        	beanNode.getParent().remove(beanNode);
	        }
	        Element beansElement = (Element)document.selectSingleNode("//Handlers");
	        Element beanElement = beansElement.addElement("bean");
	        beanElement.addAttribute("id",handlerId);
	        beanElement.addAttribute("module",moduleName);
	        
	        XmlUtil.writeDocument(document, "UTF-8", configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void generateServiceIndexConfig(String serviceId){
		try {
			String configFile = this.basePath + "ServiceIndex.xml";
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        XPath xpath = document.createXPath("//Services/bean[@id='"+serviceId+"']");
	        Node beanNode = xpath.selectSingleNode(document);
	        if (beanNode != null){
	        	beanNode.getParent().remove(beanNode);
	        }
	        Element beansElement = (Element)document.selectSingleNode("//Services");
	        Element beanElement = beansElement.addElement("bean");
	        beanElement.addAttribute("id",serviceId);
	        beanElement.addAttribute("module",moduleName);
	        
	        XmlUtil.writeDocument(document, "UTF-8", configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void generatePortletIndexConfig(String portletId){
		try {
			String configFile = this.basePath + "PortletIndex.xml";
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        XPath xpath = document.createXPath("//Portlets/bean[@id='"+portletId+"']");
	        Node beanNode = xpath.selectSingleNode(document);
	        if (beanNode != null){
	        	beanNode.getParent().remove(beanNode);
	        }
	        Element beansElement = (Element)document.selectSingleNode("//Portlets");
	        Element beanElement = beansElement.addElement("bean");
	        beanElement.addAttribute("id",portletId);
	        beanElement.addAttribute("module",moduleName);
	        
	        XmlUtil.writeDocument(document, "UTF-8", configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}

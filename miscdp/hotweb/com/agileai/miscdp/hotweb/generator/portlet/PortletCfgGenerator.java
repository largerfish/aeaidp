﻿package com.agileai.miscdp.hotweb.generator.portlet;

import java.io.File;
import java.util.HashMap;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.XPath;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.portlet.PortletFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.hotweb.generator.IndexConfigGenerator;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;
/**
 * 最简功能模型Handler配置代码生成器
 */
public class PortletCfgGenerator implements Generator{
	private PortletFuncModel funcModel = null;
	
	private String portltConfigFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(portltConfigFile));
	        addPortletElement(document);
	        XmlUtil.writeDocument(document, encoding, portltConfigFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addPortletElement(Document document){
		String portletId = funcModel.getPortletName();
        HashMap xmlMap = new HashMap();
        xmlMap.put("s", "http://java.sun.com/xml/ns/portlet/portlet-app_2_0.xsd");
        XPath xpath = document.createXPath("//s:portlet-app/s:portlet[@id='"+portletId+"']");
        xpath.setNamespaceURIs(xmlMap);
        Node beanNode = xpath.selectSingleNode(document);
        if (beanNode != null){
        	beanNode.getParent().remove(beanNode);
        }
        Element appElement = (Element)document.selectSingleNode("//portlet-app");
        Element portletElement = appElement.addElement("portlet");
        portletElement.addAttribute("id",portletId);
        portletElement.addElement("portlet-name").setText(funcModel.getPortletName());
        portletElement.addElement("display-name").addAttribute("xml:lang", "zh").setText(funcModel.getPortletTitle());
        portletElement.addElement("portlet-class").setText(funcModel.getPortletClass());
        
        Element viewPageElement = portletElement.addElement("init-param");
        viewPageElement.addElement("name").setText("ViewPage");
        viewPageElement.addElement("value").setText("/pages/"+funcModel.getViewJspName());
        
        Element editPageElement = portletElement.addElement("init-param");
        editPageElement.addElement("name").setText("EditPage");
        editPageElement.addElement("value").setText("/pages/"+funcModel.getEditJspName());
        
        if (funcModel.isGenerateHelp()){
        	Element helpPageElement = portletElement.addElement("init-param");
        	helpPageElement.addElement("name").setText("HelpPage");
        	helpPageElement.addElement("value").setText("/pages/"+funcModel.getEditJspName().substring(0,funcModel.getEditJspName().length()-8)+"Help.jsp");
        }
        
        Element supports = portletElement.addElement("supports");
        supports.addElement("mime-type").setText("text/html");
        supports.addElement("portlet-mode").setText("view");
        supports.addElement("portlet-mode").setText("edit");
        if (funcModel.isGenerateHelp()){
        	supports.addElement("portlet-mode").setText("help");	
        }
        supports.addElement("window-state").setText("normal");
        supports.addElement("window-state").setText("maximized");
        
        portletElement.addElement("supported-locale").setText("zh_CN");
        
        Element portletInfo = portletElement.addElement("portlet-info");
        portletInfo.addElement("title").setText(funcModel.getPortletTitle());
        if (StringUtil.isNotNullNotEmpty(funcModel.getKeywords())){
        	portletInfo.addElement("keywords").setText(funcModel.getKeywords());
        }
        
        IndexConfigGenerator indexConfigGenerator = new IndexConfigGenerator(funcModel.getProjectName(), funcModel.getFuncSubPkg()); 
        indexConfigGenerator.generatePortletIndexConfig(portletId);
	}
	

	public void setPortltConfigFile(String configFile) {
		this.portltConfigFile = configFile;
	}

	public void setFuncModel(PortletFuncModel suFuncModel) {
		this.funcModel = suFuncModel;
	}
}

﻿package com.agileai.miscdp.hotweb.generator.treecontent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.xml.sax.SAXException;

import com.agileai.miscdp.MiscdpPlugin;
import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.FileUtil;

import freemarker.ext.dom.NodeModel;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
/**
 * Handler代码生成器
 */
public class TACHandlerGenerator implements Generator{
	private String charencoding = "UTF-8";
	private String templateDir = null;
	
	private File xmlFile = null;
	private String srcPath = null;
	private TreeContentFuncModel treeContentFuncModel = null;
	
	public void generate() {
		String fileName = "template";
		try {
			templateDir = FileLocator.toFileURL(Platform.getBundle(MiscdpPlugin.getPluginId()).getResource(fileName)).getFile().toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
        try {
        	String listHandlerClassName = treeContentFuncModel.getListHandlerClass();
        	String listTemplateFile = "/treecontent/ListHandler.java.ftl";
        	generateJavaFile(listTemplateFile,listHandlerClassName);
        	
        	String selectTreeHandlerClassName = treeContentFuncModel.getSelectTreeHandlerClass();
        	String selectTreeHandlerTemplateFile = "/treecontent/ColumnPickHandler.java.ftl";
        	generateJavaFile(selectTreeHandlerTemplateFile,selectTreeHandlerClassName);
        	
//        	String treeEditMode = treeContentFuncModel.getTreeEditMode();
        	boolean isManageTree = treeContentFuncModel.isManageTree();
        	if (isManageTree
//        			&& TreeContentFuncModel.TreeEditMode.Popup.toString().equals(treeEditMode)
        			){
        		String treeEditHandlerClassName = treeContentFuncModel.getTreeEditHandlerClass();
        		String treeEditHandlerTemplateFile = "/treecontent/ColumnEditHandler.java.ftl";
        		generateJavaFile(treeEditHandlerTemplateFile,treeEditHandlerClassName);	
        	}
        	
        	List<ContentTableInfo> contentTableInfos = treeContentFuncModel.getContentTableInfoList();
        	for (int i=0;i < contentTableInfos.size();i++){
        		ContentTableInfo contentTableInfo = contentTableInfos.get(i);
        		String contentTableName = contentTableInfo.getTableName();
        		
        		String contentEditHandlerClass = treeContentFuncModel.getContentEditHandlerClass(contentTableName);
        		String fullHandlerClassName = treeContentFuncModel.getMainPkg()+".module."
        				+treeContentFuncModel.getFuncSubPkg()+".handler."+contentEditHandlerClass;
        		String contentEditHandlerTemplateFile = "/treecontent/ContentEditHandler.java.ftl";
        		generateJavaFile(contentEditHandlerTemplateFile,fullHandlerClassName,new Integer(i));
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void generateJavaFile(String templateFile,String fullClassName) throws IOException, 
		SAXException, ParserConfigurationException, TemplateException{
    	Configuration cfg = new Configuration();
    	cfg.setEncoding(Locale.getDefault(), charencoding);
    	cfg.setDirectoryForTemplateLoading(new File(templateDir));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
    	Template temp = cfg.getTemplate(templateFile,charencoding);
    	temp.setEncoding(charencoding);
        Map root = new HashMap();
        NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
        root.put("doc",nodeModel);
    	File javaFile = FileUtil.createJavaFile(srcPath, fullClassName);
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(javaFile),charencoding));
//        Writer out = new OutputStreamWriter(System.out);
        temp.process(root, out);
        out.flush();
        
        MiscdpUtil.getJavaFormater().format(javaFile.getAbsoluteFile(), charencoding);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void generateJavaFile(String templateFile,String fullClassName,int contentIndex) throws IOException, 
		SAXException, ParserConfigurationException, TemplateException{
    	Configuration cfg = new Configuration();
    	cfg.setEncoding(Locale.getDefault(), charencoding);
    	cfg.setDirectoryForTemplateLoading(new File(templateDir));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
    	Template temp = cfg.getTemplate(templateFile,charencoding);
    	temp.setEncoding(charencoding);
        Map root = new HashMap();
        NodeModel nodeModel = freemarker.ext.dom.NodeModel.parse(xmlFile);
        root.put("doc",nodeModel);
        root.put("contentIndex",contentIndex);
        
    	File javaFile = FileUtil.createJavaFile(srcPath, fullClassName);
        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(javaFile),charencoding));
//        Writer out = new OutputStreamWriter(System.out);
        temp.process(root, out);
        out.flush();
        
        MiscdpUtil.getJavaFormater().format(javaFile.getAbsoluteFile(), charencoding);
	}

	public void setXmlFile(File xmlFile) {
		this.xmlFile = xmlFile;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public void setTreeContentFuncModel(TreeContentFuncModel treeContentFuncModel) {
		this.treeContentFuncModel = treeContentFuncModel;
	}
}

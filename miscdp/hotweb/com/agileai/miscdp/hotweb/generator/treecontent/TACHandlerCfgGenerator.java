﻿package com.agileai.miscdp.hotweb.generator.treecontent;

import java.io.File;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import com.agileai.miscdp.NoOpEntityResolver;
import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
import com.agileai.miscdp.hotweb.generator.Generator;
import com.agileai.miscdp.hotweb.generator.IndexConfigGenerator;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.XmlUtil;
/**
 * Handler配置代码生成器
 */
public class TACHandlerCfgGenerator implements Generator{
	private TreeContentFuncModel funcModel = null;
	private String configFile = null;
	private String encoding = "UTF-8";
	public void generate() {
		try {
			IndexConfigGenerator indexConfigGenerator = new IndexConfigGenerator(funcModel.getProjectName(),funcModel.getFuncSubPkg());
			
	        SAXReader saxReader = new SAXReader();
	        saxReader.setEntityResolver(new NoOpEntityResolver());
	        saxReader.setIncludeExternalDTDDeclarations(false);
	        saxReader.setValidation(false);
	        Document document = saxReader.read(new File(configFile));
	        
	        String listHandlerId = funcModel.getListHandlerId();
	        String listHandlerClass = funcModel.getListHandlerClass();
	        String listJspPage = funcModel.getListJspName();
	        MiscdpUtil.newHandlerConfigElement(document, listHandlerId, listHandlerClass, listJspPage);
	        indexConfigGenerator.generateHandlerIndexConfig(listHandlerId);
	        
	        
	        String selectTreeHandlerId = funcModel.getSelectTreeHandlerId();
	        String selectTreeHandlerClass = funcModel.getSelectTreeHandlerClass();
	        String selectTreeJspPage = funcModel.getFuncSubPkg()+"/"+funcModel.getTreePickJspName();
	        MiscdpUtil.newHandlerConfigElement(document, selectTreeHandlerId, selectTreeHandlerClass, selectTreeJspPage);
	        indexConfigGenerator.generateHandlerIndexConfig(selectTreeHandlerId);
	        
//        	String treeEditMode = funcModel.getTreeEditMode();
        	boolean isManageTree = funcModel.isManageTree();
        	if (isManageTree){
        		String treeEditHandlerId = funcModel.getTreeEditHandlerId();
        		String treeEditHandlerClassName = funcModel.getTreeEditHandlerClass();
        		String treeEditJspName=  funcModel.getFuncSubPkg()+"/"+funcModel.getTreeEditJspName();
        		MiscdpUtil.newHandlerConfigElement(document, treeEditHandlerId, treeEditHandlerClassName, treeEditJspName);
        		indexConfigGenerator.generateHandlerIndexConfig(treeEditHandlerId);
        	}
        	
        	List<ContentTableInfo> contentTableInfos = funcModel.getContentTableInfoList();
        	for (int i=0;i < contentTableInfos.size();i++){
        		ContentTableInfo contentTableInfo = contentTableInfos.get(i);
        		String contentTableName = contentTableInfo.getTableName();
        		
        		String contentEditHandlerId = funcModel.getContentEditHandlerId(contentTableName);
        		String contentEditHandlerClass = funcModel.getMainPkg()+".module."+
        				funcModel.getFuncSubPkg()+".handler."+funcModel.getContentEditHandlerClass(contentTableName);
        		String contentEditJspName = funcModel.getFuncSubPkg()+"/"+funcModel.getContentEditJspName(contentTableName);
        		MiscdpUtil.newHandlerConfigElement(document, contentEditHandlerId, contentEditHandlerClass, contentEditJspName);
        		indexConfigGenerator.generateHandlerIndexConfig(contentEditHandlerId);
        	}
        	
	        XmlUtil.writeDocument(document, encoding, configFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}

	public void setFuncModel(TreeContentFuncModel suFuncModel) {
		this.funcModel = suFuncModel;
	}
}

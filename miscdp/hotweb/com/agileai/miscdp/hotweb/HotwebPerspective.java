package com.agileai.miscdp.hotweb;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
/**
 * Miscdp透视图
 */
public class HotwebPerspective implements IPerspectiveFactory {
	private static final String ID = "com.agileai.miscdp.hotweb.HotwebPerspective";
	
	public HotwebPerspective() {
		super();
	}
	public void createInitialLayout(IPageLayout layout) {
	    String editorArea = layout.getEditorArea();
	    IFolderLayout left = layout.createFolder("left", 1, 0.27F, editorArea);
	    left.addView("com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView");
	    left.addView("org.eclipse.jdt.ui.PackageExplorer");

	    IFolderLayout bottom = layout.createFolder("bottom", 4, 0.73F, editorArea);
	    bottom.addView("org.eclipse.ui.views.PropertySheet");
	    bottom.addView("org.eclipse.ui.console.ConsoleView");
	    bottom.addView("org.eclipse.ui.views.ProblemView");
	    bottom.addView("org.eclipse.wst.server.ui.ServersView");

	    layout.addActionSet("com.agileai.miscdp.hotweb.actionSet");

	    layout.addShowViewShortcut("com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView");
	    layout.addShowViewShortcut("org.eclipse.ui.views.PropertySheet");
	    layout.addShowViewShortcut("org.eclipse.ui.console.ConsoleView");

	    layout.addPerspectiveShortcut(ID);

	    layout.addNewWizardShortcut("com.agileai.miscdp.hotweb.ui.wizards.HotwebProjectWizard");
	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewPackageCreationWizard");
	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewClassCreationWizard");
	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewInterfaceCreationWizard");
	    layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");
	    layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");
	    
//	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewEnumCreationWizard");
//	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewAnnotationCreationWizard");
//	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewSourceFolderCreationWizard");
//	    layout.addNewWizardShortcut("org.eclipse.jdt.ui.wizards.NewSnippetFileCreationWizard");
//	    layout.addNewWizardShortcut("org.eclipse.ui.editors.wizards.UntitledTextFileWizard");
	}
}

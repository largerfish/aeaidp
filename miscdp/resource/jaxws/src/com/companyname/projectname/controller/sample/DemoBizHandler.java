package com.companyname.projectname.controller.sample;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.companyname.projectname.bizmoduler.sample.DemoBizManage;


public class DemoBizHandler
        extends BaseHandler {
    public DemoBizHandler() {
        super();
        this.serviceId = buildServiceId(DemoBizManage.class);
    }
    
    @PageAction
    public ViewRenderer sayHi(DataParam param){
    	String theGuyName = param.get("guyName","brother");
    	String responseText = getService().sayHi(theGuyName);
    	return new AjaxRenderer(responseText);
    }

    protected DemoBizManage getService() {
        return (DemoBizManage) this.lookupService(this.getServiceId());
    }
}
